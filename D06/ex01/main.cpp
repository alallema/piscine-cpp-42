/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 17:32:15 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 19:45:06 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <sstream>
#include <ctime>

struct Data {
	std::string s1;
	int n;
	std::string s2;
};

void *		serialize(void){
	std::string str;
	std::stringstream stt;
	int n;

	n = 2;
	char c;
	for (int i = 0; i < 8; i++){
		while (!std::isalnum(c = static_cast<char>(std::rand())));
		stt << c;
	}
	n = rand() % 2147483647;
	stt << n;
	for (int i = 0; i < 8; i++){
		while (!std::isalnum(c = static_cast<char>(std::rand())));
		stt << c;
	}
	str += stt.str();
	return reinterpret_cast<void *>(const_cast<char *>(str.c_str()));//(&adress of the heap);
}

Data *		deserialize(void * raw){
	Data *data = new Data();
	std::string str;

	str = reinterpret_cast<char *>(raw);
	data->s1 = str.substr(0, 8);
	data->s2 = str.substr(str.size() - 8, 8);
	data->n = std::atoi(str.substr(8, str.size() - 16).c_str());

	return data;
}

int		main(){

	std::srand(std::time(NULL) + std::clock());
	void* serial = serialize();
	Data *data = new Data();

	serial = serialize();
	data = deserialize(serial);

	std::cout << data->s1 << std::endl;
	std::cout << data->s2 << std::endl;
	std::cout << data->n << std::endl;
	return (0);
}
