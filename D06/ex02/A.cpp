/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   A.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:55:01 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:35:46 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "A.hpp"

A::A(void){
	return;
}

A::A(A const & src){
	*this = src;
	return;
}

A::~A(void) {
	return;
}

A &			A::operator=(A const & rhs){
	static_cast<void>(rhs);
	return (*this);
}
