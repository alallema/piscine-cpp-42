/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:57:38 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:37:32 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "C.hpp"

C::C(void){
	return;
}

C::C(C const & src){
	*this = src;
	return;
}

C::~C(void) {
	return;
}

C &			C::operator=(C const & rhs){
	static_cast<void>(rhs);
	return (*this);
}
