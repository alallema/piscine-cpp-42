/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   B.hpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 20:21:52 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:34:10 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef B_HPP
# define B_HPP
# include "Base.hpp"

class	B : public Base{

public:
	B(void);
	B(B const & src);
	virtual ~B(void);

	B &			operator=(B const & rhs);

};

#endif
