/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Base.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:49:58 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:35:39 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BASE_HPP
# define BASE_HPP

class	Base {

public:
	Base(void);
	Base(Base const & src);
	virtual ~Base(void);

	Base &			operator=(Base const & rhs);
};

#endif
