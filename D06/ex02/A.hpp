/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   A.hpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:54:16 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:34:03 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef A_HPP
# define A_HPP
#include "Base.hpp"

class	A : public Base{

public:
	A(void);
	A(A const & src);
	virtual ~A(void);

	A &			operator=(A const & rhs);
};

#endif
