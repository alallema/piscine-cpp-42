/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C.hpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:58:21 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:34:38 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef C_HPP
# define C_HPP
#include "Base.hpp"

class	C : public Base{

public:
	C(void);
	C(C const & src);
	virtual ~C(void);

	C &			operator=(C const & rhs);
};

#endif
