/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Base.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:52:51 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:32:55 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Base.hpp"

Base::Base(void){
	return;
}

Base::Base(Base const & src){
	*this = src;
	return;
}

Base::~Base(void) {
	return;
}

Base &			Base::operator=(Base const & rhs){
	static_cast<void>(rhs);
	return (*this);
}
