/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   B.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 20:35:51 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:37:04 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "B.hpp"

B::B(void){
	return;
}

B::B(B const & src){
	*this = src;
	return;
}

B::~B(void) {
	return;
}

B &			B::operator=(B const & rhs){
	static_cast<void>(rhs);
	return (*this);
}
