/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:59:22 by alallema          #+#    #+#             */
/*   Updated: 2018/04/04 20:53:25 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"

Base * generate(void){
	Base* base;// = new Base();
	int n = rand() % 3;

	if (n == 0)
		base = new A();
	if (n == 1)
		base = new B();
	if (n == 2)
		base = new C();
	return base;
}

void identify_from_pointer( Base * p ){
	A* baseA;
	B* baseB;
	C* baseC;

	baseA = dynamic_cast<A *>(p);
	baseB = dynamic_cast<B *>(p);
	baseC = dynamic_cast<C *>(p);
	if (baseA == NULL && baseB == NULL)
		std::cout << "C" << std::endl;
	if (baseA == NULL && baseC == NULL)
		std::cout << "B" << std::endl;
	if (baseB == NULL && baseC == NULL)
		std::cout << "A" << std::endl;
	return ;
}

void identify_from_reference( Base & p ){
	A* baseA;
	B* baseB;
	C* baseC;

	baseA = dynamic_cast<A *>(&p);
	baseB = dynamic_cast<B *>(&p);
	baseC = dynamic_cast<C *>(&p);
	if (baseA == NULL && baseB == NULL)
		std::cout << "C" << std::endl;
	if (baseA == NULL && baseC == NULL)
		std::cout << "B" << std::endl;
	if (baseB == NULL && baseC == NULL)
		std::cout << "A" << std::endl;
	return ;
}

int		main(){
	std::srand(std::time(NULL) + std::clock());
	Base *base;

	base = generate();
	identify_from_pointer(base);
	identify_from_reference(*base);
	delete base;
	return (0);
}
