/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:08:32 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 16:40:37 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent(std::string type) : type(type){
	std::srand(std::time(NULL) + std::clock());
	this->setZombieType(type);
	return;
}

ZombieEvent::~ZombieEvent(void){
	return;
}

void			ZombieEvent::setZombieType(std::string type){
	this->type = type;
	return;
}

Zombie*			ZombieEvent::newZombie(std::string name) const{
	Zombie* zombie = new Zombie(name, this->type);
	return(zombie);
}

Zombie*			ZombieEvent::randomChump() const{
	std::string names[6] = {"Zombie1", "Zombie2", "Zombie3", "Zombie4", "Zombie5", "Zombie6"};

	Zombie* zombie = this->newZombie(names[rand() % 5]);
	zombie->announce();
	return(zombie);
}
