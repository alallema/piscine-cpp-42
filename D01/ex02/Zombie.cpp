/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:05:22 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 16:44:45 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type) : _type(type), _name(name){
	std::cout << "Zombie " << name << " is born" << std::endl;
	return;
}

Zombie::~Zombie(void){
	std::cout << "Zombie " << this->_name << " is died" << std::endl;
	return;
}

void	Zombie::announce() const{
	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss..."<< std::endl;
	return;
}
