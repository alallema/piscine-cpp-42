/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:08:52 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 16:46:15 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int		main(){
	ZombieEvent event1 = ZombieEvent("event1");

	Zombie* RandomZombie = event1.randomChump();
	event1.setZombieType("type1");
	Zombie* Type1Zombie = event1.randomChump();

	ZombieEvent event2 = ZombieEvent("event2");
	Zombie* event2Zombie = event2.randomChump();

	delete RandomZombie;
	delete Type1Zombie;
	delete event2Zombie;
	return(0);
}
