/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:12:47 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 15:58:10 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_H
# define ZOMBIEEVENT_H
#include <iostream>
#include "Zombie.hpp"

class	ZombieEvent {

public:
	ZombieEvent(std::string type);
	~ZombieEvent(void);

	void			setZombieType(std::string type);
	Zombie*			newZombie(std::string name) const;
	Zombie*			randomChump() const;

	std::string		type;
};

#endif
