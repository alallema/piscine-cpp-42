/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:08:19 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 15:37:51 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_H
# define ZOMBIE_H
#include <iostream>
#include <string>

class	Zombie {

public:
	Zombie(std::string name, std::string type);
	~Zombie(void);

	void			announce(void) const;

private:
	std::string		_type;
	std::string		_name;
};

#endif
