/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 20:27:50 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 22:05:58 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP
#include "Weapon.hpp"

class	HumanB {

public:
	HumanB(std::string _name);
	~HumanB(void);

	void			setWeapon(Weapon& weapon);
	void			attack() const;

private:
	Weapon*			_weapon;
	std::string		_name;
};

#endif
