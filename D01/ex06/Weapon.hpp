/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 20:27:10 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 21:09:38 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_HPP
# define WEAPON_HPP
#include <iostream>

class	Weapon {

public:
	Weapon(std::string type);
	~Weapon(void);

	std::string const&		getType();
	void					setType(std::string type);

	std::string		type;
};

#endif
