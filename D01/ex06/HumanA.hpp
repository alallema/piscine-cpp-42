/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 20:27:34 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 21:08:33 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
# define HUMANA_HPP
#include "Weapon.hpp"

class	HumanA {

public:
	HumanA(std::string _name, Weapon& weapon);
	~HumanA(void);

	Weapon&			weapon;

	void			attack(void) const;

private:
	std::string		_name;
};

#endif
