/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 20:27:01 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 20:50:40 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

Weapon::Weapon(std::string type) : type(type) {
	return;
}

Weapon::~Weapon() {
	return;
}

std::string const&		Weapon::getType(){
	return(this->type);
}

void					Weapon::setType(std::string type){
	this->type = type;
	return;
}
