/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 20:27:44 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 22:37:34 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

HumanB::HumanB(std::string _name) : _name(_name){
	return;
}

HumanB::~HumanB(){
	return;
}

void			HumanB::setWeapon(Weapon& weapon){
	this->_weapon = &weapon;
	return;
}

void			HumanB::attack(void) const{
	std::cout << this->_name << " attacks with his " << this->_weapon->getType() << std::endl;
	return;
}
