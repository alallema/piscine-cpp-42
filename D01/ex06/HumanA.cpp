/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 20:27:26 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 21:09:36 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"

HumanA::HumanA(std::string _name, Weapon& weapon) : weapon(weapon), _name(_name){
	return;
}

HumanA::~HumanA(){
	return;
}

void	HumanA::attack(void) const{

	std::cout << this->_name << " attacks with his " << this->weapon.getType() << std::endl;
	return;
}
