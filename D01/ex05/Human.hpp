/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 17:20:58 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 19:59:42 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_H
# define HUMAN_H
#include <iostream>
#include <string>
#include "Brain.hpp"

class	Human {

public:
	Human();
	~Human(void);

	Brain const			brain;

	Brain const&		getBrain();
	std::string		identify(void) const;
};

#endif
