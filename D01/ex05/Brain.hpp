/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 17:20:37 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 20:05:40 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRAIN_H
# define BRAIN_H
#include <iostream>
#include <sstream>
#include <string>

class	Brain {

public:
	Brain();
	~Brain(void);

	std::string		identify(void) const;// return adress hexa
};

#endif
