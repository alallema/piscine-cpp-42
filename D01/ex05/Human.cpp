/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 17:20:50 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 20:13:55 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

Human::Human(){
	return;
}

Human::~Human(void){
	return;
}


Brain const&	Human::getBrain(){
	return(this->brain);
}

std::string		Human::identify(void) const{
	std::cout << "Call Human identifiy" << std::endl;
	return (this->brain.identify());
}
