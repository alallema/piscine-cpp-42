/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 17:20:30 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 20:12:48 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain(){
	return;
}

Brain::~Brain(void){
	return;
}

std::string		Brain::identify(void) const{
	std::cout << "Call Brain identifiy" << std::endl;
	std::stringstream	strstt;
	strstt << this;

	return strstt.str();
}
