/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 16:50:02 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 19:20:54 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEHORDE_H
# define ZOMBIEHORDE_H
#include <iostream>
#include <string>
#include "Zombie.hpp"

class	ZombieHorde {

public:
	ZombieHorde(int N);
	~ZombieHorde(void);

	void			announce(void) const;
	std::string		CreateName(void);
	std::string		CreateType(void);

private:
	Zombie*			_zombie;
	int				_nbrZombie;
};

#endif
