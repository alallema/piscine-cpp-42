/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:05:22 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 18:47:52 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie(void){
	std::cout << "Zombie is born" << std::endl;
	return;
}

Zombie::~Zombie(void){
	std::cout << "Zombie <" << this->_name << " (" << this->_type << ")>  is died" << std::endl;
	return;
}

void	Zombie::announce() const{
	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss..."<< std::endl;
	return;
}

void	Zombie::initZombie(std::string name, std::string type){
	this->_name = name;
	this->_type = type;
	return;
}
