/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 18:54:14 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 19:21:56 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieHorde.hpp"

int		main(){

	ZombieHorde horde1 = ZombieHorde(2);
	ZombieHorde horde2 = ZombieHorde(7);
	return (0);
}
