/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 15:08:19 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 19:19:38 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_H
# define ZOMBIE_H
#include <iostream>
#include <string>

class	Zombie {

public:
	Zombie(void);
	~Zombie(void);

	void			announce(void) const;
	void			initZombie(std::string name, std::string type);

private:
	std::string		_type;
	std::string		_name;
};

#endif
