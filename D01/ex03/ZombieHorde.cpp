/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 16:49:38 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 19:21:14 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int N) : _nbrZombie(N) {
	std::cout << "Horde of " << N << " Zombie(s)" << std::endl;
	std::srand(std::time(NULL) + std::clock());
	Zombie* zombie = new Zombie[N];

	for (int i = 0; i < N; i++) {
		zombie[i].initZombie(CreateName(), CreateType());
	}
	this->_zombie = zombie;
	this->announce();
	return;
}

ZombieHorde::~ZombieHorde(void) {
	std::cout << "Horde of Zombie(s) is died" << std::endl;
	delete [] this->_zombie;
	return;
}

std::string		ZombieHorde::CreateName(void){
	std::string names[6] = {"Zombie1", "Zombie2", "Zombie3", "Zombie4", "Zombie5", "Zombie6"};
	return(names[rand() % 5]);
}

std::string		ZombieHorde::CreateType(void){
	std::string types[6] = {"Type1", "Type2", "Type3", "Type4", "Type5", "Type6"};
	return(types[rand() % 5]);
}

void 		ZombieHorde::announce() const{
	for (int i = 0; i < this->_nbrZombie; i++) {
		this->_zombie[i].announce();
	}

	return ;
}
