/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 11:50:26 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 12:19:45 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_H
# define PONY_H
#include <iostream>
#include <string>

class	Pony {

public:
	Pony(std::string name);
	~Pony(void);

	std::string		getPonyName();

private:
	std::string		_name;
};

#endif
