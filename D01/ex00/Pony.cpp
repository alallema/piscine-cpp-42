/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 11:50:14 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 15:11:04 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

Pony::Pony(std::string name) : _name(name){
	std::cout << name << " is my Pony Name !" << std::endl;
	return;
}

Pony::~Pony(void){
	std::cout << "The pony " << this->getPonyName() << " is destroy ..." << std::endl;
	return;
}


std::string		Pony::getPonyName(){
	return this->_name;
}
