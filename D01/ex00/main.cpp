/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 11:59:40 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 12:38:34 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

static void		ponyOnTheHeap(void){
	Pony* MyPonyHeap = new Pony("MyLittlePony");

	delete MyPonyHeap;
	return;
}

static void		ponyOnTheStack(void){
	Pony MyPonyStack = Pony("MyLittleThunder");

	return;
}

int			main(){

	ponyOnTheHeap();
	ponyOnTheStack();
	return(0);
}
