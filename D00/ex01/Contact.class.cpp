/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 16:38:40 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 12:40:03 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.class.hpp"

Contact::Contact(void){
	this->_isSet = false;
	return;
}

Contact::~Contact(void){
	return;
}

void	Contact::printContact(void){
	std::cout << "FirstNameame      : " + this->firstName << std::endl;
	std::cout << "LastName          : " + this->lastName + "\n" ;
	std::cout << "NickName          : " + this->nickName + "\n";
	std::cout << "Postal Address    : " + this->postalAddress + "\n";
	std::cout << "email address     : " + this->emailAddress + "\n";
	std::cout << "Email address     : " + this->emailAddress << std::endl;
	std::cout << "Phone number      : " + this->phoneNumber << std::endl;
	std::cout << "Birthday date     : " + this->birthdayDate << std::endl;
	std::cout << "Favorite meal     : " + this->favoriteMeal << std::endl;
	std::cout << "Underwear color   : " + this->underwearColor << std::endl;
	std::cout << "Darkest secret    : " + this->darkestSecret << std::endl;
}

int		Contact::setContact(void){
	std::cout << "FirstNameame      : ";
	std::getline(std::cin, this->firstName);
	std::cout << "LastName          : ";
	std::getline(std::cin, this->lastName);
	std::cout << "NickName          : ";
	std::getline(std::cin, this->nickName);
	std::cout << "Postal Address    : ";
	std::getline(std::cin, this->postalAddress);
	std::cout << "Email Address     : ";
	std::getline(std::cin, this->emailAddress);
	std::cout << "Phone Number      : ";
	std::getline(std::cin, this->phoneNumber);
	std::cout << "Birthday Date     : ";
	std::getline(std::cin, this->birthdayDate);
	std::cout << "Favorite Meal     : ";
	std::getline(std::cin, this->favoriteMeal);
	std::cout << "Underwear Color   : ";
	std::getline(std::cin, this->underwearColor);
	std::cout << "Darkest Secret    : ";
	std::getline(std::cin, this->darkestSecret);
	std::cout << "Login             :";
	std::getline(std::cin, this->login);
	std::cout << std::endl;
	this->_isSet = true;
	return 0;
}

int		Contact::get_isSet( void ){
	return this->_isSet;
}
