/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.class.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 00:03:30 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 12:39:48 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_CLASS_H
# define CONTACT_CLASS_H
# include <iostream>
# include <string>

class	Contact {

public:
	Contact(void);
	~Contact(void);

	std::string		firstName;
	std::string		lastName;
	std::string		nickName;
	std::string		postalAddress;
	std::string		emailAddress;
	std::string		phoneNumber;
	std::string		birthdayDate;
	std::string		favoriteMeal;
	std::string		underwearColor;
	std::string		darkestSecret;
	std::string		login;

	void			printContact(void);
	int				setContact(void);
	int				get_isSet(void);
private:
	bool			_isSet;
};

#endif
