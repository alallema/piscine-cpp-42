/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 16:19:17 by alallema          #+#    #+#             */
/*   Updated: 2018/03/27 12:52:17 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.class.hpp"

std::string reduceString(std::string str)
{
	int size = str.size();
	std::string space = "";

	if (size == 10) {
		return (str);
	}
	else if (size < 10) {
		for (int i = 0; i < (10 - size); i++) {
			space += " ";
		}
		return (space + str);
	}
	else {
		str = str.substr(0, 9) + ".";
		return (str);
	}
}

bool is_number(const std::string& s)
{
	std::string::const_iterator i = s.begin();
	while (i != s.end() && std::isdigit(*i))
		++i;
	return !s.empty() && i == s.end();
}

void	search_in_book(Contact *book, int index){
	int				i;
	std::string		tmp;

	i = 0;
	while (i < 8) {
		if (book[i].get_isSet() == true) {
			std::cout << "         " << i << "|";
			std::cout << reduceString(book[i].firstName) << "|";
			std::cout << reduceString(book[i].lastName) << "|";
			std::cout << reduceString(book[i].nickName) << "|" << std::endl;
		}
		else if (i == 0) {
			std::cout << "No contact find" << std::endl;
			return ;
		}
		i++;
	}
	i = -1;
	std::cout << "Choose an index" << std::endl;
	std::getline(std::cin, tmp);
	i = std::atoi(tmp.c_str());
	if (i >= 0 && i < index && is_number(tmp) == true)
		book[i].printContact();
	return ;
}


int		main(){
	std::string	input;
	Contact		book[8];
	int			exit;
	int			i;

	i = 0;
	exit = 1;
	while (exit){
		std::cout << "Type a command (ADD, SEARCH, EXIT) :" << std::endl;
		std::getline(std::cin, input);
		if (input.compare("EXIT") == 0) {
			exit = 0;
		}
		else if (input.compare("ADD") == 0){
			if (i == 8)
				std::cout << "Too many contact in your phonebook" << std::endl;
			else {
				book[i].setContact();
				book[i].printContact();
				std::cout << std::endl << "Contact has been added" << std::endl;
				i++;
			}
		}
		else if (input.compare("SEARCH") == 0 && i > 0){
			std::cout << "     INDEX| FirstName|  LastName|  NickName|" << std::endl;
			std::cout << "----------|----------|----------|----------|" << std::endl;
			search_in_book(book, i);
		}
		else if (input.compare("SEARCH") == 0) {
			std::cout << "No contact yet" << std::endl;
		}
		else {
			std::cout << "BAD COMMAND" << std::endl;
		}
	}
	return (0);
}
