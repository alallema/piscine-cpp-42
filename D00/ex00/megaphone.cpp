/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 14:48:20 by alallema          #+#    #+#             */
/*   Updated: 2018/03/26 14:48:22 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <iostream>

int main(int ac, char **av)
{
    int i;
    int j;
    std::string  str;

    i = 1;
    j = 0;
    if (ac == 1)
        std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
    while (i < ac)
    {
        while (av[i][j] != '\0')
        {
            std::cout << (char)toupper(av[i][j]);
            j++;
        }
        j = 0;
        i++;
    }
    std::cout << std::endl;
    return (0);
}