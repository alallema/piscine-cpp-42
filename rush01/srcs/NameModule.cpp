/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NameModule.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 17:02:52 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 22:13:12 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/NameModule.hpp"
#include <unistd.h>
#include <string>

NameModule::NameModule() : AMonitorModule(2){
	this->_name = "Name";
	this->_type = false;
	this->_data[0].clear();
	this->_data[1].clear();
	return;
}

NameModule::NameModule(NameModule const & src) : AMonitorModule(2){
	*this = src;
	return;
}

NameModule::~NameModule(void) {
	return;
}

NameModule &			NameModule::operator=(NameModule const & rhs){
	this->_name = rhs.getName();
	this->_type = rhs.getType();
	this->_data = rhs.getData();
	return (*this);
}

void					NameModule::pullData(void)
{
	if (this->_data[0].empty())
	{
		char	tmp[4096];

		gethostname(tmp, 4096);
		this->_data[0].append(tmp);
		this->_data[0].insert(0, "Hostname: ");
		FILE * stream = popen("/usr/bin/whoami", "r");
		if (!(fgets(tmp, sizeof(tmp), stream)))
			return ;
		this->_data[1].append(tmp);
		this->_data[1].insert(0, "Username: ");
		this->_data[1].erase(this->_data[1].end()-1, this->_data[1].end());
		pclose(stream);
	}
	return ;
}

//Hostname
//Username
