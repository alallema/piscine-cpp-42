/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OSInfoModule.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:02:12 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 22:15:04 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OSInfoModule.hpp"
#include <cstring>

OSInfoModule::OSInfoModule(void) : AMonitorModule(4){
	this->_name = "OSInfo";
	this->_type = false;
	return;
}

OSInfoModule::OSInfoModule(OSInfoModule const & src) : AMonitorModule(4){
	*this = src;
	return;
}

OSInfoModule::~OSInfoModule(void) {
	return;
}

OSInfoModule &			OSInfoModule::operator=(OSInfoModule const & rhs){
	this->_name = rhs.getName();
	this->_type = rhs.getType();
	this->_data = rhs.getData();
	return (*this);
}

void					OSInfoModule::pullData(void) {
	char	tmp[4096];
	int		i = 0;

	if (FILE * stream = popen("/usr/sbin/system_profiler SPSoftwareDataType | grep 'System Version' -A 3", "r"))
	{
		while (fgets(tmp, sizeof(tmp), stream) != NULL)
		{
			this->_data[i].clear();
			this->_data[i].append(tmp);
			this->_data[i].erase(0, 6);
			this->_data[i].erase(this->_data[i].end() - 1, this->_data[i].end());
			if (i > 3)
				break;
			i++;
		}
		pclose(stream);
	}
	return ;
}

//	System Version:
//	Kernel Version:
//	Boot Volume:
//	Boot Mode:
//	Computer Name:
//	User Name:
