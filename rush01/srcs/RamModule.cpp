/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RamModule.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:17:53 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:45:32 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RamModule.hpp"
#include <sstream>

RamModule::RamModule() : AMonitorModule(2){
	this->_name = "Ram";
	this->_type = true;
	return;
}

RamModule::RamModule(RamModule const & src) : AMonitorModule(2){
	*this = src;
	return;
}

RamModule::~RamModule(void) {
	return;
}

RamModule &			RamModule::operator=(RamModule const & rhs){
	this->_name = rhs.getName();
	this->_type = rhs.getType();
	this->_data = rhs.getData();
	return (*this);
}

void			RamModule::pullData(){
	char	tmp[4096];

	if (FILE * stream = popen("top -l 1 -n 0 -s 0 | grep PhysMem", "r"))
	{
		if (!(fgets(tmp, sizeof(tmp), stream)))
			return ;
		this->_data[1].clear();
		this->_data[1].append(tmp + 9);
		this->_data[1].erase(this->_data[1].end() - 2, this->_data[1].end());
		this->_data[0].clear();
		this->_data[0].insert(0, "Activity: ");
		float percent = static_cast<float>(atoi(tmp + 9)) * 100
		   / (static_cast<float>(atoi(tmp + 9))
		   	+ static_cast<float>(atoi(strstr(tmp, "wired)") + 8)));
		std::ostringstream ss;
		ss << percent;
		std::string s(ss.str());
		if (!ss.fail())
			this->_data[0].append(s);
		this->_data[0].insert(this->_data[0].size(), "%");
		pclose(stream);
	}
	return;
}
