/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CPUModule.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:10:43 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:55:34 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CPUModule.hpp"

CPUModule::CPUModule(void) : AMonitorModule(5){
	this->_name = "CPU";
	this->_type = true;
	return;
}

CPUModule::CPUModule(CPUModule const & src) : AMonitorModule(5){
	*this = src;
	return;
}

CPUModule::~CPUModule(void) {
	return;
}

CPUModule &			CPUModule::operator=(CPUModule const & rhs){
	this->_name = rhs.getName();
	this->_type = rhs.getType();
	this->_data = rhs.getData();
	return (*this);
}

void			CPUModule::pullData(void){
	char	tmp[4096];

	if (FILE * stream = popen("top -l 1 -n 0 -s 0 | grep CPU", "r"))
	{
		fgets(tmp, sizeof(tmp), stream);
		this->_data[0].clear();
		this->_data[0].insert(0, "Activity: ");
		this->_data[0].append(strstr(tmp, "sys, ") + 5);
		this->_data[0].erase(this->_data[0].end() - 6, this->_data[0].end());
		pclose(stream);
	}
	if (FILE * stream = popen("system_profiler SPHardwareDataType | grep 'Processor Name' -A 3", "r"))
	{
		int i = 0;
		while (fgets(tmp, sizeof(tmp), stream) != NULL)
		{
			if (i != 3)
			{
				this->_data[i + 1].clear();
				this->_data[i + 1].append(tmp);
				this->_data[i + 1].erase(0, 6);
				this->_data[i + 1].erase(this->_data[i + 1].end() - 1, this->_data[i + 1].end());
			}
			i++;
			if (i == 4)
				break;
		}
		pclose(stream);
	}
	return;
}
