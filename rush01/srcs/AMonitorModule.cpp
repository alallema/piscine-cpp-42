/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMonitorModule.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 15:18:14 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 13:03:11 by msrun            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMonitorModule.hpp"

AMonitorModule::AMonitorModule(unsigned int vector_len) : IMonitorModule(), _data(std::vector<std::string>(vector_len)){
	return;
}

AMonitorModule::AMonitorModule(void) : IMonitorModule(){
	return;
}

AMonitorModule::AMonitorModule(AMonitorModule const & src){
	*this = src;
	return;
}

AMonitorModule::~AMonitorModule(void) {
	return;
}

AMonitorModule &		AMonitorModule::operator=(AMonitorModule const & rhs){
	this->_name = rhs.getName();
	this->_data = rhs.getData();
	this->_type = rhs.getType();
	return (*this);
}

std::string				AMonitorModule::getName(void) const{
	return this->_name;
}

std::vector<std::string>	AMonitorModule::getData(void) const{
	return this->_data;
}

bool					AMonitorModule::getType(void) const{
	return this->_type;
}
