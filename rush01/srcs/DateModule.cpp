/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DateModule.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 20:53:05 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 22:14:20 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/DateModule.hpp"

DateModule::DateModule(void) : AMonitorModule(1){
	this->_name = "Date";
	this->_type = false;
	return;
}

DateModule::DateModule(DateModule const & src) : AMonitorModule(1){
	*this = src;
	return;
}

DateModule::~DateModule(void) {
	return;
}

DateModule &			DateModule::operator=(DateModule const & rhs){
	this->_name = rhs.getName();
	this->_type = rhs.getType();
	return (*this);
}

void					DateModule::pullData(void) {
	char	tmp[4096];

	this->_data[0].append(tmp);
	FILE * stream = popen("/bin/date", "r");
	if (!(fgets(tmp, sizeof(tmp), stream)))
		return ;
	this->_data[0].clear();
	this->_data[0].append(tmp);
	this->_data[0].erase(this->_data[0].end() - 1, this->_data[0].end());
	pclose(stream);
	return;
}

//Date: Sat Apr  7 15:52:19 CEST 2018
