/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShellDisplay.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 15:01:05 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:54:12 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ShellDisplay.hpp"

ShellDisplay::ShellDisplay(void) : AMonitorDisplay(){
	std::srand(std::time(NULL) + std::clock());
	initscr();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	curs_set(0);
	start_color();
	nodelay(stdscr, TRUE);
	tmp = std::time(0);
	init_pair(1,COLOR_BLACK,COLOR_WHITE);
	init_pair(2,COLOR_WHITE,COLOR_BLACK);
	return;
}

ShellDisplay::ShellDisplay(ShellDisplay const & src){
	*this = src;
	return;
}

ShellDisplay::~ShellDisplay(void) {
	endwin();
	return;
}

ShellDisplay &			ShellDisplay::operator=(ShellDisplay const & rhs){
	this->AMonitorDisplay::operator=(rhs);
	return (*this);
}

void			ShellDisplay::refreshMap(void) const{
	int			i = 3;
	box(stdscr, 0, 0);
	std::vector<IMonitorModule *> modul = this->getModule();
	mvprintw(0, COLS / 2 - 14, "%s", modul[0]->getData()[0].c_str());
	for (std::vector<IMonitorModule*>::iterator it = modul.begin() + 1 ; it != modul.end(); ++it) {
		mvprintw(i, 2, "%s :", (*it)->getName().c_str());
		std::vector<std::string> modul2 = (*it)->getData();
		for (std::vector<std::string>::iterator it2 = modul2.begin() ; it2 != modul2.end(); ++it2) {
			mvprintw(i + 1, 4, "%s", (*it2).c_str());
			i+= 1;
		}
		i+= 3;
	}
	return;
}

void			ShellDisplay::stop(void) const{
	delwin(stdscr);
	endwin();
	exit(0);
	return ;
}
