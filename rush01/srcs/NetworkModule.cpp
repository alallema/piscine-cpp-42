/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NetworkModule.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:22:27 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:57:46 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NetworkModule.hpp"
#include <sstream>

NetworkModule::NetworkModule() : AMonitorModule(1) {
	this->_name = "Network";
	this->_type = true;
	this->_arrayThroughput[0] = " Bytes";
	this->_arrayThroughput[1] = " KBytes";
	this->_arrayThroughput[2] = " MBytes";
	this->_arrayThroughput[3] = " GBytes";
	char	tmp[4096];
	int		i = 0;

	if (FILE * stream = popen("netstat -I en0 -b", "r"))
	{
		while (fgets(tmp, sizeof(tmp), stream) != NULL)
		{
			if ((i == 2 || i == 3) && strstr(tmp, "  - "))
			{
				this->_previousBytes = atol(strstr(tmp, "  - ") + 4);
				break;
			}
			i++;
		}
		pclose(stream);
	}
	return;
}

NetworkModule::NetworkModule(NetworkModule const & src) : AMonitorModule(1){
	*this = src;
	return;
}

NetworkModule::~NetworkModule(void) {
	return;
}

NetworkModule &			NetworkModule::operator=(NetworkModule const & rhs){
	this->_name = rhs.getName();
	this->_type = rhs.getType();
	this->_data = rhs.getData();
	this->_previousBytes = rhs._previousBytes;
	return (*this);
}

void			NetworkModule::pullData() {
	char	tmp[4096];
	int		i = 0;

	if (FILE * stream = popen("netstat -I en0 -b", "r"))
	{
		this->_data[0].clear();
		while (fgets(tmp, sizeof(tmp), stream) != NULL)
		{
			if ((i == 2 || i == 3) && strstr(tmp, "  - "))
			{
				float throughput = atol(strstr(tmp, "  - ") + 4) - this->_previousBytes;

				i = 0;
				while (i < 3 && throughput > 1024)
				{
					throughput /= 1024;
					i++;
				}
				std::ostringstream ss;
				ss.precision(4);
				ss << throughput;
				std::string s(ss.str());
				if (!ss.fail())
				{
					this->_data[0].append(s);
					this->_data[0].append(this->_arrayThroughput[i]);
					this->_data[0].append("/sec        ");
					this->_previousBytes = atol(strstr(tmp, "  - ") + 4);
				}
				break;
			}
			i++;
		}
		pclose(stream);
	}
	return;
}
