/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMonitorDisplay.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 15:34:04 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 21:00:48 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMonitorDisplay.hpp"
#include "IMonitorModule.hpp"
#include "AMonitorModule.hpp"
#include "NameModule.hpp"
#include "OSInfoModule.hpp"
#include "DateModule.hpp"
#include "CPUModule.hpp"
#include "RamModule.hpp"
#include "NetworkModule.hpp"

AMonitorDisplay::AMonitorDisplay(void) : IMonitorDisplay(){
	this->_module.push_back(new DateModule());
	this->_module.push_back(new NameModule());
	this->_module.push_back(new OSInfoModule());
	this->_module.push_back(new CPUModule());
	this->_module.push_back(new RamModule());
	this->_module.push_back(new NetworkModule());
	return;
}

AMonitorDisplay::AMonitorDisplay(AMonitorDisplay const & src){
	*this = src;
	return;
}

AMonitorDisplay::~AMonitorDisplay(void) {
	std::vector<IMonitorModule *> modul = this->getModule();
	for (std::vector<IMonitorModule*>::iterator it = modul.begin(); it != modul.end(); ++it)
		delete *it;
	return;
}

AMonitorDisplay &			AMonitorDisplay::operator=(AMonitorDisplay const & rhs){
	this->_module = rhs.getModule();
	return (*this);
}

std::vector<IMonitorModule*>		AMonitorDisplay::getModule(void) const{
	return this->_module;
}

void								AMonitorDisplay::run(int ch){
	if (ch == 27)
		this->stop();
	std::vector<IMonitorModule *> modul = this->getModule();
	for (std::vector<IMonitorModule*>::iterator it = modul.begin() ; it != modul.end(); ++it)
		(*it)->pullData();
	return ;
}
