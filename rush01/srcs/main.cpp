/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/31 17:38:44 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 20:19:48 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <iostream>
#include <ctime>
#include <ncurses.h>
#include <unistd.h>
#include "../incs/IMonitorDisplay.hpp"
#include "../incs/AMonitorDisplay.hpp"
#include "../incs/AMonitorModule.hpp"
#include "../incs/ShellDisplay.hpp"
#include "../incs/GraphicDisplay.hpp"
#include "../incs/NameModule.hpp"

WINDOW *create_newwin(int height, int width, int starty, int startx)
{
	WINDOW *local_win = newwin(height, width, starty, startx);
	keypad(local_win, TRUE);
	return (local_win);
}

int			BeginDisplay(){
	int ch = -1;
	bool x = false;
	WINDOW *win_shell;
	WINDOW *win_graphic;
	win_shell = create_newwin(3, 25, LINES / 2 - 3, COLS/  2 - 12);
	win_graphic = create_newwin(3, 25, LINES / 2, COLS/  2 - 12);

	while (1) {
		ch = getch();
		box(stdscr, 0, 0);
		mvwprintw(win_shell, 1, 3, "SHELL DISPLAY");
		mvwprintw(win_graphic, 1, 3, "GRAPHIC DISPLAY");
		switch (ch)
		{
			case KEY_UP:
				x = true;
				break;
			case KEY_DOWN:
				x = false;
				break;
			case KEY_RIGHT:
				delwin(win_shell);
				delwin(win_graphic);
				if (x == false) {
					delwin(stdscr);
					endwin();
				}
				return x;
			case 27:
				delwin(win_shell);
				delwin(win_graphic);
				delwin(stdscr);
				endwin();
				exit(0);
				break;
		}
		wrefresh(stdscr);
		wrefresh(win_shell);
		wrefresh(win_graphic);
		if (x == false)
			box(win_graphic, 0, 0);
		else if (x == true)
			box(win_shell, 0, 0);
			usleep(10000);
	}
	return x;
}

void		get_sigwinch(int sig)
{
	if (sig == SIGWINCH){
		clear();
	}
	return ;
}

int		main(){
	IMonitorDisplay* display = new ShellDisplay();
	int ch = -1;
	bool x;
	WINDOW *win_shell;
	WINDOW *win_graphic;
	win_shell = create_newwin(3, 25, LINES / 2 - 3, COLS/  2 - 12);
	win_graphic = create_newwin(3, 25, LINES / 2, COLS/  2 - 12);

	signal(SIGWINCH, get_sigwinch);
	x = BeginDisplay();
	if (x == false){
		delete display;
		display = new GraphicDisplay();
		delete display;
		return (0);
	}
	while(1) {
		ch = getch();
		display->run(ch);
		display->refreshMap();
		if (ch == 27)
			display->stop();
		usleep(500000);
	}
	return (0);
}
