/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GraphicDisplay.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 16:40:59 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 20:16:14 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "GraphicDisplay.hpp"
#include <gtk/gtk.h>
#include <cstdlib>

//static void destroy (GtkWidget *widget, gpointer data);
static gboolean	update_data(GtkWidget *darea)
{
	gtk_widget_queue_draw(darea);
	return TRUE;
}

static void do_drawing(cairo_t *cr)
{
	static	std::vector<std::vector<int> > datas(3);
	static	int	place = 0;
	cairo_set_source_rgb(cr, 0, 255, 0);
	cairo_set_line_width(cr, 5);
	
	((monitor->getModule())[place + 3])->pullData();
	std::string tmp = (((monitor->getModule())[place + 3])->getData()[0]);
	if (datas[place].size() > 70)
		datas[place].erase(datas[place].begin());
	if (place == 2)
	{
		if (char const *buf = strstr(tmp.c_str(), "Bytes"))
		{
			int i = atoi(tmp.c_str());
			if (*(buf - 1) == 'K')
				i *= 1024;
			else if (*(buf - 1) == 'M')
				i *= 1024 * 1024;
			else if (*(buf - 1) == 'G')
				i *= 1024 * 1024 * 100;
			datas[place].push_back(i);
		}
	}
	else
		datas[place].push_back(atoi(tmp.c_str() + 10) * 2.5);
	for (int i = 0; datas[place].begin() + i != datas[place].end(); i++)
	{
		if (place != 2)
		{
			cairo_move_to(cr, i * 5, 250);
			cairo_line_to(cr, i * 5, 250 - (datas[place][i]));
		}
		else
		{
			int pos = 0;
			cairo_move_to(cr, i * 5, 250);
			if (datas[place][i] < 1024)
				pos = datas[place][i] * 250 / 1024;
			else if (datas[place][i] < 1024 * 1024)
				pos = datas[place][i] / 1024 * 250 / 1024 + 33;
			else if (datas[place][i] < 1024 * 1024 * 1024)
				pos = datas[place][i] / 1024 / 1024 * 250 / 1024 + 66;
			else
				pos = 249;
			cairo_line_to(cr, i * 5, (250 - pos));
		}
	}
	place = place + 1 > 2 ? 0 : place + 1;
	cairo_stroke(cr);
}

static gboolean on_draw_event(GtkWidget*, cairo_t *cr, gpointer)
{
	do_drawing(cr);
	return TRUE;
}

static GtkWidget* createDraw(){
	GtkWidget *frame;
	GtkWidget *darea;

	frame = gtk_frame_new (NULL);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 10);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
	darea = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(frame), darea);
	g_signal_connect(G_OBJECT(darea), "draw",
			G_CALLBACK(on_draw_event), NULL);
	gtk_widget_set_size_request(frame, 400, 250);
	g_timeout_add (1000, (GSourceFunc)update_data, darea);
	return frame;
}

static gboolean rewrite1(GtkWidget *label)
{
	std::string qwe;
	monitor->getModule()[0]->pullData();
	std::vector<std::string> data = monitor->getModule()[0]->getData();
	for (std::vector<std::string>::iterator it = data.begin(); it != data.end(); ++it)
	{
		qwe.append(*it);
		qwe.append("\n");
	}
	gtk_label_set_text (GTK_LABEL(label), qwe.c_str());
	return TRUE;
}

static gboolean rewrite4(GtkWidget *label)
{
	std::string qwe;
	std::vector<std::string> data = monitor->getModule()[3]->getData();
	for (std::vector<std::string>::iterator it = data.begin(); it != data.end(); ++it)
	{
		qwe.append(*it);
		qwe.append("\n");
	}
	gtk_label_set_text (GTK_LABEL(label), qwe.c_str());
	return TRUE;
}

static gboolean rewrite5(GtkWidget *label)
{
	std::string qwe;
	std::vector<std::string> data = monitor->getModule()[4]->getData();
	for (std::vector<std::string>::iterator it = data.begin(); it != data.end(); ++it)
	{
		qwe.append(*it);
		qwe.append("\n");
	}
	gtk_label_set_text (GTK_LABEL(label), qwe.c_str());
	return TRUE;
}

static gboolean rewrite6(GtkWidget *label)
{
	std::string qwe;
	std::vector<std::string> data = monitor->getModule()[5]->getData();
	for (std::vector<std::string>::iterator it = data.begin(); it != data.end(); ++it)
	{
		qwe.append(*it);
		qwe.append("\n");
	}
	gtk_label_set_text (GTK_LABEL(label), qwe.c_str());
	return TRUE;
}

static GtkWidget* createFrame(std::vector<std::string> data){
	static int count = 0;
	GtkWidget *frame;
	GtkWidget *label;
	std::string tmp;
	std::vector<void *> tab;
	for (std::vector<std::string>::iterator it = data.begin(); it != data.end(); ++it) {
		tmp.append(*it);
		tmp.append("\n");
	}
	frame = gtk_frame_new (NULL);
	label = gtk_label_new (tmp.c_str());
	gtk_container_set_border_width (GTK_CONTAINER (frame), 10);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
	gtk_widget_set_size_request(frame, 100, 20);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_container_add (GTK_CONTAINER (frame), label);
	gtk_frame_set_label_align (GTK_FRAME (frame), 0, 1.0);
	if (count == 0)
		g_timeout_add (1000, (GSourceFunc)rewrite1, label);
	else if (count == 3)
		g_timeout_add (1000, (GSourceFunc)rewrite4, label);
	else if (count == 4)
		g_timeout_add (1000, (GSourceFunc)rewrite5, label);
	else if (count == 5)
		g_timeout_add (1000, (GSourceFunc)rewrite6, label);
	count++;
	return frame;
}

static GtkWidget* createBox(IMonitorModule* module){
	GtkWidget *frame;
	GtkWidget *box;

	module->pullData();
	frame = gtk_frame_new (NULL);
	box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 10);
	gtk_frame_set_label (GTK_FRAME (frame), module->getName().c_str());
	gtk_frame_set_label_align (GTK_FRAME (frame), 0.5, 1.0);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_OUT);
	gtk_container_add (GTK_CONTAINER (frame), box);
	if (module->getType() == true)
		gtk_container_add (GTK_CONTAINER (box), createDraw());
	gtk_container_add (GTK_CONTAINER (box), createFrame(module->getData()));
	return frame;
}

GraphicDisplay::GraphicDisplay(void) : AMonitorDisplay()
{
	GtkWidget *window;
	GtkWidget *box;
	monitor = this;

	gtk_init(0, NULL);

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add (GTK_CONTAINER (window), box);

	gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);

	g_signal_connect(window, "destroy",
			G_CALLBACK(gtk_main_quit), NULL);

	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 400, 800);
	gtk_window_set_title(GTK_WINDOW(window), "Graphic Monitor");

	std::vector<IMonitorModule *> modul = this->getModule();
	for (std::vector<IMonitorModule*>::iterator it = modul.begin(); it != modul.end(); ++it) {
		gtk_container_add (GTK_CONTAINER (box), createBox(*it));
	}

	gtk_widget_show_all(window);

	gtk_main();
	return ;
}

//static void destroy (GtkWidget *widget, gpointer data)
//{
//	(void)widget;
//	(void)data;
//	gtk_main_quit ();
//}

GraphicDisplay::GraphicDisplay(GraphicDisplay const & src){
	*this = src;
	return;
}

GraphicDisplay::~GraphicDisplay(void) {
	return;
}

GraphicDisplay &			GraphicDisplay::operator=(GraphicDisplay const & rhs){
	(void)rhs;
	return (*this);
}

void			GraphicDisplay::refreshMap(void) const{
	return;
}

void			GraphicDisplay::run(int){
	return;
}

void			GraphicDisplay::stop(void) const{
	return;
}
