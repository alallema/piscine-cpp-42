/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NetworkModule.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:21:37 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:41:55 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NETWORKMODULE_HPP
# define NETWORKMODULE_HPP
#include <iostream>
#include "AMonitorModule.hpp"

class	NetworkModule : public AMonitorModule{

	public:
		NetworkModule(void);
		NetworkModule(NetworkModule const & src);
		~NetworkModule(void);

		NetworkModule &		operator=(NetworkModule const & rhs);
		void				pullData(void);

	protected:

	private:
		long				_previousBytes;
		std::string			_arrayThroughput[4];
};

#endif
