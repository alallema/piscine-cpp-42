/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShellDisplay.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 15:01:28 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:42:43 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ShellDISPLAY_HPP
# define ShellDISPLAY_HPP
#include <iostream>
#include <ctime>
#include <ncurses.h>
#include "AMonitorDisplay.hpp"

class	ShellDisplay : public AMonitorDisplay {

public:
	ShellDisplay(void);
	ShellDisplay(ShellDisplay const & src);
	virtual ~ShellDisplay(void);

	ShellDisplay &			operator=(ShellDisplay const & rhs);

	void					refreshMap(void) const;
	void					stop() const;

protected:

private:
};

#endif
