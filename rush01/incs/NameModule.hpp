/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NameModule.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 17:10:22 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 22:12:28 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NAMEMODULE_HPP
# define NAMEMODULE_HPP
#include <iostream>
#include "AMonitorModule.hpp"

class	NameModule : public AMonitorModule{

public:
	NameModule(void);
	NameModule(NameModule const & src);
	virtual ~NameModule(void);

	NameModule &		operator=(NameModule const & rhs);
	void				pullData(void);

protected:

private:
};

#endif
