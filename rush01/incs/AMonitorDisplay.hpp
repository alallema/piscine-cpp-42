/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMonitorDisplay.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 15:16:00 by alallema          #+#    #+#             */
/*   Updated: 2018/04/07 22:04:48 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMONITORDISPLAY_HPP
# define AMONITORDISPLAY_HPP
#include <iostream>
#include <gtk/gtk.h>
#include <vector>
#include "IMonitorDisplay.hpp"
#include "IMonitorModule.hpp"

class	AMonitorDisplay : public IMonitorDisplay{

public:
	AMonitorDisplay(void);
	AMonitorDisplay(AMonitorDisplay const & src);
	virtual ~AMonitorDisplay(void);

	AMonitorDisplay &				operator=(AMonitorDisplay const & rhs);
	virtual void					refreshMap(void) const = 0;
	virtual void					stop() const = 0;
	std::vector<IMonitorModule*>	getModule(void) const;
	void							run(int ch);

protected:
	time_t							tmp;
	std::vector<IMonitorModule*>	_module;

private:
};

#endif
