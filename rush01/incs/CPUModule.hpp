/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CPUModule.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:13:12 by alallema          #+#    #+#             */
/*   Updated: 2018/04/06 23:14:28 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPUMODULE_HPP
# define CPUMODULE_HPP
#include <iostream>
#include "AMonitorModule.hpp"

class	CPUModule : public AMonitorModule{

public:
	CPUModule(void);
	CPUModule(CPUModule const & src);
	~CPUModule(void);

	CPUModule &			operator=(CPUModule const & rhs);
	void				pullData(void);

protected:

private:
};

#endif
