/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GraphicDisplay.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 16:39:42 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 20:16:26 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GraphicDISPLAY_HPP
# define GraphicDISPLAY_HPP
#include <iostream>
#include "AMonitorDisplay.hpp"

class	GraphicDisplay : public AMonitorDisplay{

public:
	GraphicDisplay(void);
	GraphicDisplay(GraphicDisplay const & src);
	virtual ~GraphicDisplay(void);

	GraphicDisplay &	operator=(GraphicDisplay const & rhs);

	void					refreshMap(void) const;
	void					run(int ch) ;
	void					stop() const;

protected:

private:
};

static AMonitorDisplay *monitor;

#endif
