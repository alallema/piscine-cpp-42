/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RamModule.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:19:09 by alallema          #+#    #+#             */
/*   Updated: 2018/04/06 23:19:57 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAMMODULE_HPP
# define RAMMODULE_HPP
#include <iostream>
#include "AMonitorModule.hpp"

class	RamModule : public AMonitorModule {

public:
	RamModule(void);
	RamModule(RamModule const & src);
	~RamModule(void);

	RamModule &			operator=(RamModule const & rhs);
	void				pullData(void);

protected:

private:
};

#endif
