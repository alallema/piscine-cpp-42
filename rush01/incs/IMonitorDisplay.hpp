/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMonitorDisplay.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 14:44:58 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:41:09 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IMONITORDISPLAY_HPP
# define IMONITORDISPLAY_HPP
#include <iostream>
#include "IMonitorModule.hpp"

class	IMonitorDisplay {

public:
	virtual void						refreshMap(void) const = 0;
	virtual void							run(int ch) = 0;
	virtual void							stop(void) const = 0;
	virtual std::vector<IMonitorModule*>	getModule(void) const = 0;
	virtual ~IMonitorDisplay(void){};

};

#endif

// Class Shell UI Display
// Class Graphic UI Display
