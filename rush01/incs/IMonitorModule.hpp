/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMonitorModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 14:33:32 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:41:24 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IMONITORMODULE_HPP
# define IMONITORMODULE_HPP
#include <iostream>

class	IMonitorModule {

public:

	virtual std::string					getName(void) const = 0;
	virtual std::vector<std::string>	getData(void) const = 0;
	virtual bool						getType(void) const = 0;
	virtual void						pullData(void) = 0;
	virtual ~IMonitorModule(void){};
protected:
};

#endif
