/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OSInfoModule.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 23:03:46 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:42:05 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OSINFOMODULE_HPP
# define OSINFOMODULE_HPP
#include <iostream>
#include "AMonitorModule.hpp"

class	OSInfoModule : public AMonitorModule{

public:
	OSInfoModule(void);
	OSInfoModule(OSInfoModule const & src);
	virtual ~OSInfoModule(void);

	OSInfoModule &		operator=(OSInfoModule const & rhs);
	void				pullData(void);

protected:

private:
};

#endif
