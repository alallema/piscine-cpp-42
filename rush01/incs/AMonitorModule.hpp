/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMonitorModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 14:47:17 by alallema          #+#    #+#             */
/*   Updated: 2018/04/08 16:39:50 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMONITORMODULE_HPP
# define AMONITORMODULE_HPP
#include <iostream>
#include <vector>
#include "IMonitorModule.hpp"

class	AMonitorModule : public IMonitorModule{

public:
	AMonitorModule(unsigned int);
	AMonitorModule(AMonitorModule const & src);
	virtual ~AMonitorModule(void);

	AMonitorModule &		operator=(AMonitorModule const & rhs);
	std::string				getName(void) const;
	std::vector<std::string>	getData(void) const;
	bool					getType(void) const;
	virtual void			pullData(void) = 0;

protected:
	std::string					_name;
	std::vector<std::string>	_data;
	bool						_type;

private:
	AMonitorModule(void);
};

#endif
