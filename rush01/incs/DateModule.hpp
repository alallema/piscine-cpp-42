/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DateModule.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 20:55:03 by alallema          #+#    #+#             */
/*   Updated: 2018/04/06 20:55:58 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATEMODULE_HPP
# define DATEMODULE_HPP
#include <iostream>
#include "AMonitorModule.hpp"

class	DateModule : public AMonitorModule{

public:
	DateModule(void);
	DateModule(DateModule const & src);
	~DateModule(void);

	DateModule &		operator=(DateModule const & rhs);
	void				pullData(void);

protected:

private:
};

#endif
