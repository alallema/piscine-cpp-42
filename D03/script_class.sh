#!/bin/bash

##    author	    Amelie Lallemand
##    mail	    alallema@student.42.fr
##    version	    1.0, 28/03/2018
##    description
#	Boilerplate template for C++ Class


if [ ! -f $1 ]; then
	var=$(echo $1 | tr '[:lower:]' '[:upper:]')
	touch $1".cpp"
	touch $1".hpp"
	echo "/*
**
**    author		Amelie Lallemand
**    mail		alallema@student.42.fr
**    template_version	1.0, 28/03/2018
**
*/

#ifndef "$var"_HPP
# define "$var"_HPP
#include <iostream>
#include <sstream>
#include <fstream>

class	"$1" {

public:
	"$1"(void);
	"$1"(int const n);
	"$1"($1 const & src);
	~"$1"(void);

	"$1" &			operator=("$1" const & rhs);
	std::string		get"$1"();

private:
	int				_n;
};

#endif" >> $1".hpp"
	echo "#include \""$1".hpp\"

	"$1"::"$1"(void) : _ (0){
	std::cout << \"Default Constructor called\" << std::endl;
	return;
}

"$1"::"$1"(int const n) : _ (n){
	std::cout << \"Parametric Constructor called\" << std::endl;
	return;
}

"$1"::"$1"("$1" const & src){
	std::cout << \"Copy Constructor called\" << std::endl;
	*this = src;
	return;
}

"$1"::~"$1"(void) {
	std::cout << \"Default Constructor called\" << std::endl;
	return;
}

int		"$1"::get"$1"(){	
	return(this->);
}

" >> $1".cpp"
fi
