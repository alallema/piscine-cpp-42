/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 20:46:37 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:53:29 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP
#include <iostream>

class	ScavTrap {

public:
	ScavTrap(void);
	ScavTrap(std::string name);
	ScavTrap(ScavTrap const & src);
	~ScavTrap(void);

	ScavTrap &			operator=(ScavTrap const & rhs);

	int				gethitPoints(void) const;
	int				getmaxHitPoints(void) const;
	int				getenergyPoints(void) const;
	int				getmaxEnergyPoints(void) const;
	int				getlevel(void) const;
	std::string		getname(void) const;
	int				getmeleeAttackDamage(void) const;
	int				getrangedAttackDamage(void) const;
	int				getArmorDamageReduction(void) const;

	void			rangedAttack(std::string const & target);
	void			meleeAttack(std::string const & target);
	void			beRepaired(unsigned int amount);
	void			takeDamage(unsigned int amount);
	bool			isAlive(void) const;

	void			challengeNewcomer(void);

protected:
	unsigned int				hitPoints;
	unsigned int				maxHitPoints;
	unsigned int				energyPoints;
	unsigned int				maxEnergyPoints;
	int				level;
	std::string		name;
	int				meleeAttackDamage;
	int				rangedAttackDamage;
	int				ArmorDamageReduction;
};

#endif
