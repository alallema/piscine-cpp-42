/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:33 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:40:59 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(std::string name) : hitPoints(100), maxHitPoints(100), energyPoints(100), maxEnergyPoints(100), level(1), name(name), meleeAttackDamage(30), rangedAttackDamage(20), ArmorDamageReduction(5) {
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is FR4G-TP " << std::endl << "Directive one: Protect humanity! "<< name << std::endl;
	return;
}

FragTrap::FragTrap(void) : hitPoints(100), maxHitPoints(100), energyPoints(100), maxEnergyPoints(100), level(1), name("No Name"), meleeAttackDamage(30), rangedAttackDamage(20), ArmorDamageReduction(5) {
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is FR4G-TP without name" << std::endl;
	return;
}

FragTrap::FragTrap(FragTrap const & src){
	std::cout << "Copy of FR4G-TP" << std::endl;
	this->name = getname();
	*this = src;
	return;
}

FragTrap::~FragTrap(void) {
	std::cout << "Are you god? Am I dead?" << std::endl;
	return;
}

FragTrap &			FragTrap::operator=(FragTrap const & rhs) {
	this->hitPoints = rhs.gethitPoints();
	this->maxHitPoints = rhs.getmaxHitPoints();
	this->energyPoints = rhs.getenergyPoints();
	this->maxEnergyPoints = rhs.getmaxEnergyPoints();
	this->level = rhs.getlevel();
	this->name = rhs.getname();
	this->meleeAttackDamage = rhs.getmeleeAttackDamage();
	this->rangedAttackDamage = rhs.getrangedAttackDamage();
	this->ArmorDamageReduction = rhs.getArmorDamageReduction();
	return(*this);
}

void		FragTrap::rangedAttack(std::string const & target){
	std::cout << "FR4G-TP <" << this->name << "> attacks <" << target << "> at range, causing <" << this->rangedAttackDamage << "> points of damage !" << std::endl;
	return;
}

void		FragTrap::meleeAttack(std::string const & target){
	std::cout << "FR4G-TP <" << this->name <<"> attacks <" << target << "> at range, causing <" << this->meleeAttackDamage << "> points of damage !" << std::endl;
	return;
}

void		FragTrap::beRepaired(unsigned int amount){
	if (this->isAlive()) {
		if (this->hitPoints < this->maxHitPoints - amount)
			this->hitPoints += amount;
		else
			this->hitPoints = this->maxHitPoints;
		std::cout << "FR4G-TP <" << this->name << "> has win <" << amount << "> hitPoints ! His hitPoints is now at " << this->hitPoints << std::endl;
	}
	else
		std::cout << "To late " << this->name << " is Already dead" << std::endl;
	return;
}

void		FragTrap::takeDamage(unsigned int amount){
	if (this->isAlive()) {
		if (this->hitPoints >= amount - ArmorDamageReduction)
			this->hitPoints -= amount - ArmorDamageReduction;
		else
			this->hitPoints = 0;
		std::cout << "FR4G-TP <" << this->name << "> has lost <" << amount << "> points of damage ! Less his Armor of " << this->ArmorDamageReduction << " Reduction. His hitPoints is now at " << this->hitPoints << std::endl;
	}
	else
		std::cout << "To late " << this->name << " is Already dead" << std::endl;
	return;
}

bool			FragTrap::isAlive(void) const{
	if (this->hitPoints > 0)
		return true;
	else
		return false;
}

void			FragTrap::vaulthunter_dot_exe(std::string const & target){
	std::string randomAttack[6] = {"coolAttack", "superMegaPunchAttack", "hyperGigaKickAttack", "deathAttack", "shinyAttack"};
	if (this->isAlive()) {
		if (this->energyPoints >= 25){
			std::cout << this->name << " do " << randomAttack[rand() % 5] << " on " << target << std::endl;
			this->energyPoints -= 25;
		}
		else
			std::cout << "Not enough energy points ... Too bad" << std::endl;
	}
	else
		std::cout << this->name << " is dead" << std::endl;
	return;
}

int			FragTrap::gethitPoints(void) const {
	return this->hitPoints;
}

int			FragTrap::getmaxHitPoints(void) const{
	return this->maxHitPoints;
}

int			FragTrap::getenergyPoints(void) const{
	return this->energyPoints;
}

int			FragTrap::getmaxEnergyPoints(void) const{
	return this->maxEnergyPoints;
}

int			FragTrap::getlevel(void) const{
	return this->level;
}

std::string	FragTrap::getname(void) const{
	return this->name;
}

int			FragTrap::getmeleeAttackDamage(void) const{
	return this->meleeAttackDamage;
}

int			FragTrap::getrangedAttackDamage(void) const{
	return this->rangedAttackDamage;
}

int			FragTrap::getArmorDamageReduction(void) const{
	return this->ArmorDamageReduction;
}
