/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:33 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:57:15 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string name) : hitPoints(100), maxHitPoints(100), energyPoints(50), maxEnergyPoints(50), level(1), name(name), meleeAttackDamage(20), rangedAttackDamage(15), ArmorDamageReduction(3) {
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is SC4V-TP " << name << std::endl << "Directive two: Obey Jack at all costs." << std::endl;
	return;
}

ScavTrap::ScavTrap(void) : hitPoints(100), maxHitPoints(100), energyPoints(100), maxEnergyPoints(50), level(1), name("No Name"), meleeAttackDamage(20), rangedAttackDamage(15), ArmorDamageReduction(3) {
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is SC4V-TP without name" << std::endl;
	return;
}

ScavTrap::ScavTrap(ScavTrap const & src){
	std::cout << "Copy of SC4V-TP" << std::endl;
	this->name = getname();
	*this = src;
	return;
}

ScavTrap::~ScavTrap(void) {
	std::cout << "Are you god? Am I dead?" << std::endl;
	return;
}

ScavTrap &			ScavTrap::operator=(ScavTrap const & rhs) {
	this->hitPoints = rhs.gethitPoints();
	this->maxHitPoints = rhs.getmaxHitPoints();
	this->energyPoints = rhs.getenergyPoints();
	this->maxEnergyPoints = rhs.getmaxEnergyPoints();
	this->level = rhs.getlevel();
	this->name = rhs.getname();
	this->meleeAttackDamage = rhs.getmeleeAttackDamage();
	this->rangedAttackDamage = rhs.getrangedAttackDamage();
	this->ArmorDamageReduction = rhs.getArmorDamageReduction();
	return(*this);
}

void		ScavTrap::rangedAttack(std::string const & target){
	std::cout << "SC4V-TP <" << this->name << "> attacks <" << target << "> at range, causing <" << this->rangedAttackDamage << "> points of damage !" << std::endl;
	return;
}

void		ScavTrap::meleeAttack(std::string const & target){
	std::cout << "SC4V-TP <" << this->name <<"> attacks <" << target << "> at range, causing <" << this->meleeAttackDamage << "> points of damage !" << std::endl;
	return;
}

void		ScavTrap::beRepaired(unsigned int amount){
	if (this->isAlive()) {
		if (this->hitPoints < this->maxHitPoints - amount)
			this->hitPoints += amount;
		else
			this->hitPoints = this->maxHitPoints;
		std::cout << "SC4V-TP <" << this->name << "> has win <" << amount << "> hitPoints ! His hitPoints is now at " << this->hitPoints << std::endl;
	}
	else
		std::cout << "To late " << this->name << " is Already dead" << std::endl;
	return;
}

void		ScavTrap::takeDamage(unsigned int amount){
	if (this->isAlive()) {
		if (this->hitPoints >= amount)
			this->hitPoints -= amount;
		else
			this->hitPoints = 0;
		std::cout << "SC4V-TP <" << this->name << "> has lost <" << amount << "> points of damage ! His hitPoints is now at " << this->hitPoints << std::endl;
	}
	else
		std::cout << "To late " << this->name << " is Already dead" << std::endl;
	return;
}

bool			ScavTrap::isAlive(void) const{
	if (this->hitPoints > 0)
		return true;
	else
		return false;
}

void			ScavTrap::challengeNewcomer(void){
	std::string Challenge[6] = {"coolChallenge", "funnyChallenge", "hyperGigaChallenge", "deathChallenge", "shinyChallenge"};
	if (this->isAlive()) {
		std::cout << this->name << " do " << Challenge[rand() % 5] << " on NewComer" << std::endl;
	}
	else
		std::cout << this->name << " is dead" << std::endl;
	return;
}

int			ScavTrap::gethitPoints(void) const {
	return this->hitPoints;
}

int			ScavTrap::getmaxHitPoints(void) const{
	return this->maxHitPoints;
}

int			ScavTrap::getenergyPoints(void) const{
	return this->energyPoints;
}

int			ScavTrap::getmaxEnergyPoints(void) const{
	return this->maxEnergyPoints;
}

int			ScavTrap::getlevel(void) const{
	return this->level;
}

std::string	ScavTrap::getname(void) const{
	return this->name;
}

int			ScavTrap::getmeleeAttackDamage(void) const{
	return this->meleeAttackDamage;
}

int			ScavTrap::getrangedAttackDamage(void) const{
	return this->rangedAttackDamage;
}

int			ScavTrap::getArmorDamageReduction(void) const{
	return this->ArmorDamageReduction;
}
