/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:26:38 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 20:40:24 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int		main(){
	FragTrap	*Test1 = new FragTrap("ClapTrap FR4G-TP");
	ScavTrap	*Test2 = new ScavTrap("ClapTrap SC4V-TP");

	Test1->rangedAttack("Jack");
	Test1->vaulthunter_dot_exe("Jack");
	Test1->meleeAttack("Jack");
	Test1->vaulthunter_dot_exe("Jack");
	Test1->takeDamage(5);
	Test1->vaulthunter_dot_exe("Jack");
	Test1->beRepaired(10);
	Test1->vaulthunter_dot_exe("Jack");


	Test2->rangedAttack("Jack");
	Test2->meleeAttack("Jack");
	Test2->challengeNewcomer();
	Test2->takeDamage(5);
	Test2->challengeNewcomer();
	Test2->beRepaired(10);

	delete Test1;
	delete Test2;
	return (0);
}
