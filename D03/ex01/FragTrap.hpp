/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:41 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 19:55:44 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP
#include <iostream>

class	FragTrap {

public:
	FragTrap(void);
	FragTrap(std::string name);
	FragTrap(FragTrap const & src);
	~FragTrap(void);

	FragTrap &			operator=(FragTrap const & rhs);

	int				gethitPoints(void) const;
	int				getmaxHitPoints(void) const;
	int				getenergyPoints(void) const;
	int				getmaxEnergyPoints(void) const;
	int				getlevel(void) const;
	std::string		getname(void) const;
	int				getmeleeAttackDamage(void) const;
	int				getrangedAttackDamage(void) const;
	int				getArmorDamageReduction(void) const;

	void			rangedAttack(std::string const & target);
	void			meleeAttack(std::string const & target);
	void			beRepaired(unsigned int amount);
	void			takeDamage(unsigned int amount);
	bool			isAlive(void) const;

	void			vaulthunter_dot_exe(std::string const & target);

protected:
	unsigned int				hitPoints;
	unsigned int				maxHitPoints;
	unsigned int				energyPoints;
	unsigned int				maxEnergyPoints;
	int				level;
	std::string		name;
	int				meleeAttackDamage;
	int				rangedAttackDamage;
	int				ArmorDamageReduction;
};

#endif
