/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:26:38 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:36:31 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

int		main(){
	FragTrap	*Test1 = new FragTrap("ClapTrap");

	Test1->rangedAttack("Jack");
	Test1->vaulthunter_dot_exe("Jack");
	Test1->meleeAttack("Jack");
	Test1->vaulthunter_dot_exe("Jack");
	Test1->takeDamage(25);
	Test1->vaulthunter_dot_exe("Jack");
	Test1->beRepaired(10);
	Test1->vaulthunter_dot_exe("Jack");
	Test1->vaulthunter_dot_exe("Jack");

	delete Test1;
	return (0);
}
