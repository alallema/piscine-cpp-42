/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:33 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:40:59 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ClapTrap.hpp"

FragTrap::FragTrap(std::string name) : ClapTrap(name){
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 100;
	this->maxEnergyPoints = 100;
	this->level = 1;
	this->meleeAttackDamage = 30;
	this->rangedAttackDamage = 20;
	this->ArmorDamageReduction = 5;
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is FR4G-TP " << std::endl << "Directive one: Protect humanity! "<< name << std::endl;
	return;
}

FragTrap::FragTrap(void){
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is FR4G-TP without name" << std::endl;
	return;
}

FragTrap::FragTrap(FragTrap const & src) : ClapTrap(src.getname())
{
	std::cout << "Copy of FR4G-TP" << std::endl;
	this->name = getname();
	*this = src;
	return;
}

FragTrap::~FragTrap(void) {
	std::cout << "Are you god? Am I dead?" << std::endl;
	return;
}

FragTrap &			FragTrap::operator=(FragTrap const & rhs) {
	this->hitPoints = rhs.gethitPoints();
	this->maxHitPoints = rhs.getmaxHitPoints();
	this->energyPoints = rhs.getenergyPoints();
	this->maxEnergyPoints = rhs.getmaxEnergyPoints();
	this->level = rhs.getlevel();
	this->name = rhs.getname();
	this->meleeAttackDamage = rhs.getmeleeAttackDamage();
	this->rangedAttackDamage = rhs.getrangedAttackDamage();
	this->ArmorDamageReduction = rhs.getArmorDamageReduction();
	return(*this);
}

void		FragTrap::rangedAttack(std::string const & target){
	std::cout << "FR4G-TP <" << this->name << "> attacks <" << target << "> at range, causing <" << this->rangedAttackDamage << "> points of damage !" << std::endl;
	return;
}

void		FragTrap::meleeAttack(std::string const & target){
	std::cout << "FR4G-TP <" << this->name <<"> attacks <" << target << "> at range, causing <" << this->meleeAttackDamage << "> points of damage !" << std::endl;
	return;
}

void			FragTrap::vaulthunter_dot_exe(std::string const & target){
	std::string randomAttack[6] = {"coolAttack", "superMegaPunchAttack", "hyperGigaKickAttack", "deathAttack", "shinyAttack"};
	if (this->isAlive()) {
		if (this->energyPoints >= 25){
			std::cout << this->name << " do " << randomAttack[rand() % 5] << " on " << target << std::endl;
			this->energyPoints -= 25;
		}
		else
			std::cout << "Not enough energy points ... Too bad" << std::endl;
	}
	else
		std::cout << this->name << " is dead" << std::endl;
	return;
}
