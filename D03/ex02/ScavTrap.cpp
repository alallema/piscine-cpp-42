/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:33 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:57:15 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string name) : ClapTrap(name){
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 50;
	this->maxEnergyPoints = 50;
	this->level = 1;
	this->meleeAttackDamage = 20;
	this->rangedAttackDamage = 15;
	this->ArmorDamageReduction = 3;
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is SC4V-TP " << name << std::endl << "Directive two: Obey Jack at all costs." << std::endl;
	return;
}

ScavTrap::ScavTrap(void){
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is SC4V-TP without name" << std::endl;
	return;
}

ScavTrap::ScavTrap(ScavTrap const & src) : ClapTrap(src.getname()){
	std::cout << "Copy of SC4V-TP" << std::endl;
	*this = src;
	return;
}

ScavTrap::~ScavTrap(void) {
	std::cout << "Are you god? Am I dead?" << std::endl;
	return;
}

ScavTrap &			ScavTrap::operator=(ScavTrap const & rhs) {
	this->hitPoints = rhs.gethitPoints();
	this->maxHitPoints = rhs.getmaxHitPoints();
	this->energyPoints = rhs.getenergyPoints();
	this->maxEnergyPoints = rhs.getmaxEnergyPoints();
	this->level = rhs.getlevel();
	this->name = rhs.getname();
	this->meleeAttackDamage = rhs.getmeleeAttackDamage();
	this->rangedAttackDamage = rhs.getrangedAttackDamage();
	this->ArmorDamageReduction = rhs.getArmorDamageReduction();
	return(*this);
}

void		ScavTrap::rangedAttack(std::string const & target){
	std::cout << "SC4V-TP <" << this->name << "> attacks <" << target << "> at range, causing <" << this->rangedAttackDamage << "> points of damage !" << std::endl;
	return;
}

void		ScavTrap::meleeAttack(std::string const & target){
	std::cout << "SC4V-TP <" << this->name <<"> attacks <" << target << "> at range, causing <" << this->meleeAttackDamage << "> points of damage !" << std::endl;
	return;
}

void			ScavTrap::challengeNewcomer(void){
	std::string Challenge[6] = {"coolChallenge", "funnyChallenge", "hyperGigaChallenge", "deathChallenge", "shinyChallenge"};
	if (this->isAlive()) {
		std::cout << this->name << " do " << Challenge[rand() % 5] << " on NewComer" << std::endl;
	}
	else
		std::cout << this->name << " is dead" << std::endl;
	return;
}
