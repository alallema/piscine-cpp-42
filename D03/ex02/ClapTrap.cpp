/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:33 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 21:40:59 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap(std::string name) : name(name) {
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Clap Clap ClapTrap" << std::endl << "Directive three: Dance! "<< name << std::endl;
	return;
}

ClapTrap::ClapTrap(void) : name("No Name") {
	std::srand(std::time(NULL) + std::clock());
	std::cout << "Your new designation is ClapTrap without name" << std::endl;
	return;
}

ClapTrap::~ClapTrap(void) {
	std::cout << "Bye ClapTrap" << std::endl;
	return;
}

ClapTrap &			ClapTrap::operator=(ClapTrap const & rhs) {
	this->hitPoints = rhs.gethitPoints();
	this->maxHitPoints = rhs.getmaxHitPoints();
	this->energyPoints = rhs.getenergyPoints();
	this->maxEnergyPoints = rhs.getmaxEnergyPoints();
	this->level = rhs.getlevel();
	this->name = rhs.getname();
	this->meleeAttackDamage = rhs.getmeleeAttackDamage();
	this->rangedAttackDamage = rhs.getrangedAttackDamage();
	this->ArmorDamageReduction = rhs.getArmorDamageReduction();
	return(*this);
}

void		ClapTrap::beRepaired(unsigned int amount){
	if (this->isAlive()) {
		if (this->hitPoints < this->maxHitPoints - amount)
			this->hitPoints += amount;
		else
			this->hitPoints = this->maxHitPoints;
		std::cout << "ClapTrap <" << this->name << "> has win <" << amount << "> hitPoints ! His hitPoints is now at " << this->hitPoints << std::endl;
	}
	else
		std::cout << "To late " << this->name << " is Already dead" << std::endl;
	return;
}

void		ClapTrap::takeDamage(unsigned int amount){
	if (this->isAlive()) {
		if (this->hitPoints >= amount - ArmorDamageReduction)
			this->hitPoints -= amount - ArmorDamageReduction;
		else
			this->hitPoints = 0;
		std::cout << "ClapTrap <" << this->name << "> has lost <" << amount << "> points of damage ! Less his Armor of " << this->ArmorDamageReduction << " Reduction. His hitPoints is now at " << this->hitPoints << std::endl;
	}
	else
		std::cout << "To late " << this->name << " is Already dead" << std::endl;
	return;
}

bool			ClapTrap::isAlive(void) const{
	if (this->hitPoints > 0)
		return true;
	else
		return false;
}

int			ClapTrap::gethitPoints(void) const {
	return this->hitPoints;
}

int			ClapTrap::getmaxHitPoints(void) const{
	return this->maxHitPoints;
}

int			ClapTrap::getenergyPoints(void) const{
	return this->energyPoints;
}

int			ClapTrap::getmaxEnergyPoints(void) const{
	return this->maxEnergyPoints;
}

int			ClapTrap::getlevel(void) const{
	return this->level;
}

std::string	ClapTrap::getname(void) const{
	return this->name;
}

int			ClapTrap::getmeleeAttackDamage(void) const{
	return this->meleeAttackDamage;
}

int			ClapTrap::getrangedAttackDamage(void) const{
	return this->rangedAttackDamage;
}

int			ClapTrap::getArmorDamageReduction(void) const{
	return this->ArmorDamageReduction;
}
