/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 19:16:41 by alallema          #+#    #+#             */
/*   Updated: 2018/03/29 19:55:44 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP
#include <iostream>

class	ClapTrap {

public:
	ClapTrap(void);
	ClapTrap(std::string name);
	ClapTrap(ClapTrap const & src);
	~ClapTrap(void);

	ClapTrap &			operator=(ClapTrap const & rhs);

	int				gethitPoints(void) const;
	int				getmaxHitPoints(void) const;
	int				getenergyPoints(void) const;
	int				getmaxEnergyPoints(void) const;
	int				getlevel(void) const;
	std::string		getname(void) const;
	int				getmeleeAttackDamage(void) const;
	int				getrangedAttackDamage(void) const;
	int				getArmorDamageReduction(void) const;

	void			beRepaired(unsigned int amount);
	void			takeDamage(unsigned int amount);
	bool			isAlive(void) const;

protected:
	unsigned int				hitPoints;
	unsigned int				maxHitPoints;
	unsigned int				energyPoints;
	unsigned int				maxEnergyPoints;
	int				level;
	std::string		name;
	int				meleeAttackDamage;
	int				rangedAttackDamage;
	int				ArmorDamageReduction;
};

#endif
