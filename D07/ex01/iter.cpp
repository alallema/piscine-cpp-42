#include <iostream>

template< typename T >
void	iter(T *array, int lenght, void (*func) (T &)) {

	for (int i = 0; i < lenght; i++) {
		func(array[i]);
	}
	return ;
}

template< typename T >
void print(T str) {
	std::cout << str << std::endl;
}

int main( void ) {
	std::string string[3] = {"lala", "petit", "chat"};
	char c[3] = {42, 'b', 'c'};
	int numberInt[3] = {1, 2, 3};
	float numberFloat[3] = {1.32, 2.45, 3.012};

	::iter(string, 3, print);
	::iter(c, 3, print);
	::iter(numberInt, 3, print);
	::iter(numberFloat, 3, print);
	return 0;
}

