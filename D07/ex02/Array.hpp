/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:09:47 by alallema          #+#    #+#             */
/*   Updated: 2018/04/05 18:54:39 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
# define ARRAY_HPP
#include <iostream>
#include <sstream>
#include <fstream>

template< typename T >
class	Array {

public:

	Array<T>(void){
		this->array = new T[0]();
		this->len = 0;
		return ;
	}

	Array<T>(unsigned int n){
		this->array = new T[n]();
		this->len = n;
		return ;
	}

	Array<T>(Array const & src){
//		*this = src;
		this->array = src.array;
		this->len = src.len;
		return ;
	}

	~Array<T>(void){
		delete [] this->array;
	}

	Array <T>&			operator=(Array const & rhs){
		this->array = new T(rhs.size);
		this->len = rhs.size();
		for (int i = 0; i < this->len ; i++){
			this->array[i] = rhs.array[i];
		}
	}

	T&			operator[](const int i){
		if (this->len > i)
			return this->array[i];
		else
			throw std::exception();
	}

	int			size(void){
		return this->len;
	}
private:
	T*			array;
	int			len;
};

#endif
