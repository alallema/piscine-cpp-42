/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:18:46 by alallema          #+#    #+#             */
/*   Updated: 2018/04/05 18:59:47 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"

int		main() {
	Array<int>				A;
	Array<int>				B(4);
	Array<std::string>		C(4);

	for (int i = 0; i < B.size(); i++)
		B[i] = i;
	for (int i = 0; i < B.size(); i++)
		std::cout << B[i] << " ";
	std::cout << std::endl;
	B[1] = 8;
	for (int i = 0; i < B.size(); i++)
		std::cout << B[i] << " ";
	std::cout << std::endl;
	C[0] = "lala";
	C[1] = "petit";
	C[2] = "chat";
	C[3] = "!";
	for (int i = 0; i < C.size(); i++)
		std::cout << C[i] << " ";
	std::cout << std::endl;
	return (0);
}
