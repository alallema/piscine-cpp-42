/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 18:18:41 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 04:03:52 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Intern.hpp"


int main( void ) {
	Intern someRandomIntern;

	std::cout << "------------" << std::endl;
	Form* rrf;
	Form* scf;
	Form* ppf;

	rrf = someRandomIntern.makeForm("robotomy request", "Bender");
	scf = someRandomIntern.makeForm("shrubbery creation", "Raymond");
	ppf = someRandomIntern.makeForm("presidential pardon", "Bob");

	delete rrf;
	delete scf;
	delete ppf;
return 0;
}
