/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 00:24:35 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 03:33:24 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form(target, 145, 137){
	return;
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const & src) : Form(src){
	*this = src;
	return;
}

ShrubberyCreationForm::~ShrubberyCreationForm(void) {
	return;
}

ShrubberyCreationForm &			ShrubberyCreationForm::operator=(ShrubberyCreationForm const & rhs){
	if(this != &rhs)
		*this = rhs;
	return (*this);
}

void					ShrubberyCreationForm::execute(Bureaucrat const & executor) const{
	if (!this->getSigned()){
		std::cout << "Form " << this->getName() <<" not signed" << std::endl;
		return;
	}
	if (executor.getGrade() >= this->getGradeEx()){
		throw Form::GradeTooHighException();
		return;
	}
	std::string fileName = (this->getName()+ "_shrubbery");
	std::ofstream newFile;
	newFile.open(fileName.c_str());
	if (newFile.is_open()){
		newFile << "              # #### ####" << std::endl
		<< "            ### \\/#|### |/####" << std::endl
		<< "           ##\\/#/ \\||/##/_/##/_#" << std::endl
		<< "         ###  \\/###|/ \\/ # ###" << std::endl
		<< "       ##_\\_#\\_\\## | #/###_/_####" << std::endl
		<< "      ## #### # \\ #| /  #### ##/##" << std::endl
		<< "       __#_--###`  |{,###---###-~" << std::endl
		<< "                 \\ }{" << std::endl
		<< "                  }}{" << std::endl
		<< "                  }}{" << std::endl
		<< "             ejm  {{}" << std::endl
		<< "            , -=-~{ .-^- _" << std::endl
		<< "                   `}" << std::endl
		<< "                   {" << std::endl;
	}
	else {
		std::cout << "Problem to open File" << std::endl;
	}
	newFile.close();
	return;
}
