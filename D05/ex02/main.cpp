/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 18:18:41 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 04:05:25 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"


int main( void ) {

	try {
		Bureaucrat comptable("Robert", 10);
		ShrubberyCreationForm comptabilite("Compta");
		comptabilite.beSigned(comptable);
		comptabilite.execute(comptable);
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Bureaucrat" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Bureaucrat" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}

	std::cout << "------------" << std::endl;
	try {
		Bureaucrat comptable("Robert", 10);
		RobotomyRequestForm comptabilite("Compta");
		comptabilite.beSigned(comptable);
		comptabilite.execute(comptable);
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Bureaucrat" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Bureaucrat" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}

	std::cout << "------------" << std::endl;
	try {
		Bureaucrat comptable("Robert", 3);
		PresidentialPardonForm comptabilite("Compta");
		comptabilite.beSigned(comptable);
		comptabilite.execute(comptable);
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Bureaucrat" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Bureaucrat" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}

	std::cout << "------------" << std::endl;
	try {
		Bureaucrat comptable("Robert", 6);
		PresidentialPardonForm comptabilite("Compta");
		comptabilite.beSigned(comptable);
		comptable.upGrade();
		std::cout << comptable;
		comptable.executeForm(comptabilite);
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Bureaucrat" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Bureaucrat" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}
return 0;
}
