/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 00:31:21 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 03:48:55 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(std::string target) : Form(target, 25, 5){
	return;
}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const & src) : Form(src){
	*this = src;
	return;
}

PresidentialPardonForm::~PresidentialPardonForm(void) {
	return;
}

PresidentialPardonForm &			PresidentialPardonForm::operator=(PresidentialPardonForm const & rhs){
	if(this != &rhs)
		*this = rhs;
	return (*this);
}

void					PresidentialPardonForm::execute(Bureaucrat const & executor) const{
	if (!this->getSigned()){
		std::cout << "Form " << this->getName() <<" not signed" << std::endl;
		return;
	}
	if (executor.getGrade() > this->getGradeEx()){
		throw Form::GradeTooHighException();
		return;
	}
	std::cout << executor.getName() << " has been pardoned by Zaphod Beeblebrox." << std::endl;
	return;
}
