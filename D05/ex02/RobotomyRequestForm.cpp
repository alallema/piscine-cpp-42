/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 00:29:07 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 03:33:08 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(std::string target) : Form(target, 72, 45){
	std::srand(std::time(NULL) + std::clock());
	return;
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const & src) : Form(src){
	*this = src;
	return;
}

RobotomyRequestForm::~RobotomyRequestForm(void) {
	return;
}

RobotomyRequestForm &			RobotomyRequestForm::operator=(RobotomyRequestForm const & rhs){
	if(this != &rhs)
		*this = rhs;
	return (*this);
}

void					RobotomyRequestForm::execute(Bureaucrat const & executor) const{
	if (!this->getSigned()){
		std::cout << "Form " << this->getName() <<" not signed" << std::endl;
		return;
	}
	if (executor.getGrade() >= this->getGradeEx()){
		throw Form::GradeTooHighException();
		return;
	}
	if (rand() % 2 == 0){
		std::cout << this->getName() << " has been robotomized failure."<< std::endl;
		return;
	}
	else {
		std::cout << this->getName() << " has been robotomized successfully"<< std::endl;
		std::cout << "RIZZ RIZZZ" << std::endl;
		std::cout << "CRRR CRRR ZZZZ" << std::endl;
	}
	return;
}
