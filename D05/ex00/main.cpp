/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 18:18:41 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 03:50:17 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"


int main( void ) {
	try {
		Bureaucrat* comptable = new Bureaucrat("Robert", 17);
		comptable->upGrade();
		std::cout << *comptable;
		delete comptable;
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException" << std::endl;
	}
	try {
		std::cout << "------------" << std::endl;
		Bureaucrat* comptable = new Bureaucrat("Raymond", 1);
		std::cout << *comptable;
		comptable->upGrade();
		delete comptable;
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException" << std::endl;
	}
	try {
		std::cout << "------------" << std::endl;
		Bureaucrat* comptable = new Bureaucrat("Bertrand", 150);
		std::cout << *comptable;
		comptable->downGrade();
		delete comptable;
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException" << std::endl;
	}
return 0;
}
