/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 18:18:41 by alallema          #+#    #+#             */
/*   Updated: 2018/04/03 03:50:02 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Bureaucrat.hpp"


int main( void ) {
	try {
		Bureaucrat comptable("Robert", 10);
		Form* comptabilite = new Form("compta", 17, 16);
		comptable.upGrade();
		std::cout << comptable;
		comptabilite->beSigned(comptable);
		comptabilite->beSigned(comptable);
		delete comptabilite;
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Bureaucrat" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Bureaucrat" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}
	try {
		std::cout << "------------" << std::endl;
		Bureaucrat comptable("Robert", 20);
		Form* comptabilite = new Form("compta", 17, 16);
		std::cout << comptable;
		comptabilite->beSigned(comptable);
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}
	try {
		std::cout << "------------" << std::endl;
		Bureaucrat comptable("Robert", 10);
		Form* comptabilite = new Form("compta", 170, 16);
		comptable.upGrade();
		std::cout << comptable;
		comptabilite->beSigned(comptable);
		comptabilite->beSigned(comptable);
		delete comptabilite;
	}
	catch (Bureaucrat::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Bureaucrat" << std::endl;
	}
	catch (Bureaucrat::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Bureaucrat" << std::endl;
	}
	catch (Form::GradeTooLowException& e) {
		std::cout << "GradeTooLowException Form" << std::endl;
	}
	catch (Form::GradeTooHighException& e) {
		std::cout << "GradeTooHighException Form" << std::endl;
	}
return 0;
}
