/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 17:48:29 by alallema          #+#    #+#             */
/*   Updated: 2018/04/02 23:58:44 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP
#include <iostream>
#include "Form.hpp"

class Form;

class	Bureaucrat {

public:
	Bureaucrat(std::string name, int grade);
	Bureaucrat(Bureaucrat const & src);
	~Bureaucrat(void);

	Bureaucrat &		operator=(Bureaucrat const & rhs);
	std::string			getName(void) const;
	int					getGrade(void) const;
	void				upGrade();
	void				downGrade();
	void				signForm(Form& src);

	class GradeTooHighException : public std::exception{

	public:
		GradeTooHighException(void) throw();
		GradeTooHighException(GradeTooHighException const & src) throw();
		virtual ~GradeTooHighException(void) throw();

		GradeTooHighException& operator=(GradeTooHighException const & rhs);
	};

	class GradeTooLowException : public std::exception{

	public:
		GradeTooLowException(void) throw();
		GradeTooLowException(GradeTooLowException const & src) throw();
		virtual ~GradeTooLowException(void) throw();

		GradeTooLowException& operator=(GradeTooLowException const & rhs);
	};

protected:
	Bureaucrat(void);

private:
	const std::string	_name;
	int					_grade;//1 to 150
};

std::ostream & operator<<(std::ostream & o, Bureaucrat const & rhs);

#endif
