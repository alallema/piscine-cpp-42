#!/bin/bash
if [ ! -f $1 ]; then
	var=$(echo $1 | tr '[:lower:]' '[:upper:]')
	touch $1".cpp"
	touch $1".hpp"
	echo "#ifndef "$var"_HPP
# define "$var"_HPP
#include <iostream>
#include <sstream>
#include <fstream>

class	"$1" {

public:
	"$1"(std::string str);
	"$1"($1 const & src);
	~"$1"(void);

	"$1" &			operator=("$1" const & rhs);
	std::string		getstr(void) const;

protected:
	"$1"(void);

private:
	std::string		_str;
};

#endif" >> $1".hpp"
	echo "#include \""$1".hpp\"

"$1"::"$1"(std::string str) : _str (str){
	std::cout << \"Parametric Constructor called\" << std::endl;
	return;
}

"$1"::"$1"("$1" const & src){
	std::cout << \"Copy Constructor called\" << std::endl;
	*this = src;
	return;
}

"$1"::~"$1"(void) {
	std::cout << \"Destructor called\" << std::endl;
	return;
}

"$1" &			"$1"::operator=("$1" const & rhs){
	this->str = rhs.getstr();
	return (*this);
}

std::string		"$1"::getstr(){
	return(this->str);
}

" >> $1".cpp"
fi
