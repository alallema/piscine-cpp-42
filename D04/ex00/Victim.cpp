/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 13:35:13 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 16:42:52 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim(std::string name) : _name(name){
	std::cout << "Some random victim called " << name << " just popped !" << std::endl;
	return;
}

Victim::Victim(Victim const & src){
	std::cout << "Some random victim called " << src.getName() << " just popped !" << std::endl;
	*this = src;
	return;
}

Victim::~Victim(void) {
	std::cout << "Victim " << this->_name << " just died for no apparent reason !" << std::endl;
	return;
}

Victim &		Victim::operator=(Victim const & rhs){
	this->_name = rhs.getName();
	return (*this);
}

std::string		Victim::getName() const{
	return(this->_name);
}

void			Victim::getPolymorphed(void) const{
	std::cout << this->_name << " has been turned into a cute little sheep !" << std::endl;
	return;
}

std::ostream & operator<<(std::ostream & o, Victim const & rhs){
	o << "I'm " << rhs.getName() << " and I like otters !" << std::endl;
	return(o);
}
