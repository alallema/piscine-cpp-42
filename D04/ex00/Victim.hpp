/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 13:35:37 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 16:34:17 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
# define VICTIM_HPP
#include <iostream>

class	Victim {

public:
	Victim(std::string name);
	Victim(Victim const & src);
	virtual ~Victim(void);

	Victim &			operator=(Victim const & rhs);
	std::string			getName(void) const;
	virtual void		getPolymorphed(void) const;

protected:
	Victim(void);

private:
	std::string			_name;
};

std::ostream & operator<<(std::ostream & o, Victim const & rhs);

#endif
