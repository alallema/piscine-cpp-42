/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 13:35:13 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 16:44:17 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

Sorcerer::Sorcerer(std::string name, std::string title) : _name(name), _title(title){
	std::cout << name << ", "<< title << ", is born !" << std::endl;
	return;
}

Sorcerer::Sorcerer(Sorcerer const & src){
	std::cout << src.getName() << ", "<< src.getTitle() << ", is born !" << std::endl;
	*this = src;
	return;
}

Sorcerer::~Sorcerer(void) {
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same !" << std::endl;
	return;
}

Sorcerer &			Sorcerer::operator=(Sorcerer const & rhs){
	this->_name = rhs.getName();
	this->_title = rhs.getTitle();
	return (*this);
}

std::string		Sorcerer::getName() const{
	return(this->_name);
}

std::string		Sorcerer::getTitle() const{
	return(this->_title);
}

void			Sorcerer::polymorph(Victim const & victim) const{
	victim.getPolymorphed();
	return;
}

std::ostream &		operator<<(std::ostream & o, Sorcerer const & rhs){
	o << "I'm " << rhs.getName() << ", " << rhs.getTitle() << ", and I like ponies !" << std::endl;
	return(o);
}
