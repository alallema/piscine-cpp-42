/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 13:35:13 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 16:57:46 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

Peon::Peon(std::string name) : Victim(name){
	std::cout << "Zog zog." << std::endl;
	return;
}

Peon::Peon(Peon const & src) : Victim(src){
	std::cout << "Some random victim called " << src.getName() << " just popped !" << std::endl;
	*this = src;
	return;
}

Peon::~Peon(void) {
	std::cout << "Bleuark..." << std::endl;
	return;
}

Peon &		Peon::operator=(Peon const & rhs){
	this->getName() = rhs.getName();
	return (*this);
}

void		Peon::getPolymorphed(void) const{
	std::cout << this->getName() << " has been turned into a pink pony !" << std::endl;
	return;
}
