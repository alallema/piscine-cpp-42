/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 13:35:37 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 17:25:26 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
# define SORCERER_HPP
#include <iostream>
#include "Victim.hpp"


class	Sorcerer {

public:
	Sorcerer(std::string name, std::string title);
	Sorcerer(Sorcerer const & src);
	~Sorcerer(void);

	Sorcerer &			operator=(Sorcerer const & rhs);
	std::string			getName(void) const;
	std::string			getTitle(void) const;
	void				introduce();
	void				polymorph(Victim const & src) const;

protected:
	Sorcerer(void);

private:
	std::string			_name;
	std::string			_title;
};

std::ostream & operator<<(std::ostream & o, Sorcerer const & rhs);

#endif
