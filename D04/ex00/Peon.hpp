/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 13:35:37 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 16:52:52 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
# define PEON_HPP
#include <iostream>
#include "Victim.hpp"

class	Peon : public Victim {

public:
	Peon(std::string name);
	Peon(Peon const & src);
	virtual ~Peon(void);

	Peon &			operator=(Peon const & rhs);
	virtual void	getPolymorphed(void) const;

protected:
	Peon(void);
};

#endif
