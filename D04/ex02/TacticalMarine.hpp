/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 23:30:58 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 23:31:00 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TACTICALMARINE_HPP
# define TACTICALMARINE_HPP
#include <iostream>

class	TacticalMarine {

public:
	TacticalMarine(std::string str);
	TacticalMarine(TacticalMarine const & src);
	~TacticalMarine(void);

	TacticalMarine &			operator=(TacticalMarine const & rhs);
	std::string		getstr(void) const;

protected:
	TacticalMarine(void);

private:
	std::string		_str;
};

#endif
