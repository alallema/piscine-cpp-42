/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 23:21:28 by alallema          #+#    #+#             */
/*   Updated: 2018/03/31 00:12:38 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
# define SQUAD_HPP
#include <iostream>
#include "ISpaceMarine.hpp"
#include "ISquad.hpp"

class	Squad: public ISquad {

typedef struct		s_unit
{
	ISpaceMarine*	marine;
	struct s_unit	*next;
}					t_unit;

public:
	Squad(int _nbUnit, t_unit* _beginUnit);
	Squad(Squad const & src);
	~Squad(void);

	Squad &			operator=(Squad const & rhs);
	int				getCount() const;
	int				push(ISpaceMarine*);
	ISpaceMarine*	getUnit(int) const;
//	std::string		getstr(void) const;

private:
	int			_nbUnit;
	t_unit*		_beginUnit;
	Squad(void);
};

#endif
