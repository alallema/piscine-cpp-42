/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 23:21:40 by alallema          #+#    #+#             */
/*   Updated: 2018/03/31 00:15:31 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"

Squad::Squad(int _nbUnit, t_unit* _beginUnit) : _beginUnit(NULL), _nbUnit(0) {
//	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

Squad::Squad(Squad const & src){
//	std::cout << "Copy Constructor called" << std::endl;
	*this = src;
	return;
}

Squad::~Squad(void) {
//	std::cout << "Destructor called" << std::endl;
	return;
}

//Squad &			Squad::operator=(Squad const & rhs){
//	this->getCount() = rhs.getCount();
//	return (*this);
//}



int				Squad::getCount() const{
//returns the number of units currently in the Squad
	return (this->_nbUnit);
}

ISpaceMarine*	Squad::getUnit(int) const{
// list end;
//returns a pointer to the Nth unit (Of course, we start at 0. Null
//		pointer in case of out-of-bounds index.)
	return(this->_unit);
}
int				Squad::push(ISpaceMarine*){

	return();
}
