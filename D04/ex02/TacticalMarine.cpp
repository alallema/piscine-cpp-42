#include "TacticalMarine.hpp"

TacticalMarine::TacticalMarine(std::string str) : _str (str){
	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

TacticalMarine::TacticalMarine(TacticalMarine const & src){
	std::cout << "Copy Constructor called" << std::endl;
	*this = src;
	return;
}

TacticalMarine::~TacticalMarine(void) {
	std::cout << "Destructor called" << std::endl;
	return;
}

TacticalMarine &			TacticalMarine::operator=(TacticalMarine const & rhs){
	this->str = rhs.getstr();
	return (*this);
}

std::string		TacticalMarine::getstr(){
	return(this->str);
}


