/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 17:28:11 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 18:46:42 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
# define AWEAPON_HPP
#include <iostream>

class AWeapon {
private:
	std::string		_name;
	int				_apcost;
	int				_damage;

	AWeapon(void);

public:
	AWeapon(std::string const & name, int apcost, int damage);
	AWeapon(AWeapon const & src);
	virtual ~AWeapon(void);

	std::string		getName() const;
	int				getAPCost() const;
	int				getDamage() const;
	void			setName(std::string name);
	void			setAPCost(int apcost);
	void			setDamage(int damage);
	virtual void attack() const = 0;
};

#endif
