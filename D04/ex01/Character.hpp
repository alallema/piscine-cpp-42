/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 21:19:36 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 22:42:58 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP
#include <iostream>
#include "AWeapon.hpp"
#include "Enemy.hpp"

class	Character {

public:
	Character(std::string const name);
	Character(Character const & src);
	~Character(void);

	Character &		operator=(Character const & rhs);
	void			recoverAP();
	void			equip(AWeapon*);
	void			attack(Enemy* &);
	std::string		getName() const;
	int				getAp() const;
	AWeapon*		getWeapon() const;
	void			setName(std::string name);
	void			setAp(int ap);
	void			setWeapon(bool armed);
	bool			armed(void) const;

protected:
	Character(void);

private:
	std::string		_name;
	int				_ap;
	AWeapon*		_weapon;
	bool			_armed;
};

std::ostream & operator<<(std::ostream & o, Character const & rhs);

#endif
