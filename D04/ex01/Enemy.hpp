/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 19:13:33 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 20:57:42 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
# define ENEMY_HPP
#include <iostream>

class Enemy {

private:
	Enemy(void);
//	int				_damage;

protected:
	std::string		_type;
	int				_hp;

public:
	Enemy(int hp, std::string const & type);
	Enemy(Enemy const & src);
	virtual ~Enemy(void);

//	Enemy &			operator=(Enemy const & rhs);

	std::string		getType() const;
	int				getHP() const;
	void			setType(std::string type);
	void			setHP(int hp);
	virtual void	takeDamage(int);
};

#endif
