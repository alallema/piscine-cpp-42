/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 18:49:37 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 18:56:19 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLASMARIFLE_HPP
# define PLASMARIFLE_HPP
#include <iostream>
#include "AWeapon.hpp"

class	PlasmaRifle : public AWeapon{

public:
	PlasmaRifle(void);
	PlasmaRifle(PlasmaRifle const & src);
	virtual ~PlasmaRifle(void);

	PlasmaRifle &	operator=(PlasmaRifle const & rhs);
	void			attack() const;
//	std::string		getstr(void) const;

private:
//	std::string		_str;
};

#endif
