/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 21:23:56 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 22:44:09 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character(std::string name) : _name (name), _ap(40){
//	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

Character::Character(Character const & src){
//	std::cout << "Copy Constructor called" << std::endl;
	*this = src;
	return;
}

Character::~Character(void) {
//	std::cout << "Destructor called" << std::endl;
	return;
}

Character &		Character::operator=(Character const & rhs){
	this->setName(rhs.getName());
	this->setAp(rhs.getAp());
	this->setWeapon(rhs.getWeapon());
	return (*this);
}

std::string		Character::getName(void) const{
	return(this->_name);
}

int				Character::getAp(void) const{
	return(this->_ap);
}

AWeapon*		Character::getWeapon(void) const{
	return(this->_weapon);
}

void			Character::setName(std::string name){
	this->_name = name;
	return;
}

void			Character::setAp(int ap){
	this->_ap = ap;
	return;
}

void			Character::setWeapon(bool armed){
	this->_armed = armed;
	return;
}

void			Character::recoverAP(void){
	if (this->getAp() <= 40 - 10)
		this->setAp(getAp() + 10);
	else
		this->setAp(40);
	return;
}

void			Character::equip(AWeapon* weapon){
	this->setWeapon(true);
	this->_weapon = weapon;
	return;
}

void			Character::attack(Enemy* & enemy){
//	NAME attacks ENEMY_TYPE with a WEAPON_NAME
	if (enemy && enemy != NULL && enemy != 0) {
		if (this->armed() && this->_ap >= this->_weapon->getAPCost()) {
			this->setAp(this->_ap - this->_weapon->getAPCost());
			std::cout << this->getName() << " attacks " << enemy->getType() << " with a " << this->_weapon->getName() << std::endl;
			this->_weapon->attack();
			enemy->takeDamage(this->_weapon->getDamage());
			if (enemy->getHP() == 0) {
				std::cout << enemy->getType() << " is dead" << std::endl;
				delete enemy;
				enemy = NULL;
			}

			return ;
		}
		else {
			if (this->armed()) {
				std::cout << "Not enough AP" << std::endl;
			}
			else {
				std::cout << "No weapon" << std::endl;
			}
		}
	}
	else {
		std::cout << "To late !" << std::endl;
	}

	return ;

	return;
}

bool			Character::armed() const{
	return this->_armed;
}

std::ostream &		operator<<(std::ostream & o, Character const & rhs){
//	NAME has AP_NUMBER AP and wields a WEAPON_NAME
	if (rhs.armed()) {
		o << rhs.getName() <<" has " << rhs.getAp() << " AP and wields a " << rhs.getWeapon()->getName() << std::endl;
	}
	else {
		o << rhs.getName() <<" has " << rhs.getAp() << " AP and is unarmed" << std::endl;
	}
//	NAME attacks ENEMY_TYPE with a WEAPON_NAME
	return(o);
}
