/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 19:10:02 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 21:09:24 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"

PowerFist::PowerFist(void) : AWeapon("Power Fist", 8, 50){
//	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

PowerFist::PowerFist(PowerFist const & src) : AWeapon(src){
//	std::cout << "Copy Constructor called" << std::endl;
	*this = src;
	return;
}

PowerFist::~PowerFist(void) {
//	std::cout << "Destructor called" << std::endl;
	return;
}

PowerFist &			PowerFist::operator=(PowerFist const & rhs){
	this->setName(rhs.getName());
	this->setAPCost(rhs.getAPCost());
	this->setDamage(rhs.getDamage());
	return (*this);
}

void			PowerFist::attack(void) const{
	std::cout << "* pschhh... SBAM! *" << std::endl;
	return;
}
