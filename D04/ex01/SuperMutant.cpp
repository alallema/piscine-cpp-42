/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 21:06:04 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 21:16:26 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"

SuperMutant::SuperMutant(void) : Enemy(170, "Super Mutant") {
	std::cout << "“Gaaah. Me want smash heads !" << std::endl;
	return;
}

SuperMutant::SuperMutant(SuperMutant const & src) : Enemy(src){
	std::cout << "“Gaaah. Me want smash heads !" << std::endl;
	*this = src;
	return;
}

SuperMutant::~SuperMutant(void) {
	std::cout << "Aaargh ..." << std::endl;
	return;
}

SuperMutant &	SuperMutant::operator=(SuperMutant const & rhs){
	this->setHP(rhs.getHP());
	this->setType(rhs.getType());
	return (*this);
}

void			SuperMutant::takeDamage(int damage){
	damage = damage - 3;
	if (damage > 0)
	{
		if (this->getHP() - damage >= 0)
			this->setHP(this->getHP() - damage);
		else
			this->setHP(0);
	}
	return ;
}
