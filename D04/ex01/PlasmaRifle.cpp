/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 18:56:38 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 21:08:52 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle(void) : AWeapon("Plasma Rifle", 5, 21){
//	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

PlasmaRifle::PlasmaRifle(PlasmaRifle const & src) : AWeapon(src){
	*this = src;
	return;
}

PlasmaRifle::~PlasmaRifle(void) {
//	std::cout << "Destructor called" << std::endl;
	return;
}

PlasmaRifle &			PlasmaRifle::operator=(PlasmaRifle const & rhs){
	this->setName(rhs.getName());
	this->setAPCost(rhs.getAPCost());
	this->setDamage(rhs.getDamage());
	return (*this);
}

void			PlasmaRifle::attack(void) const{
	std::cout << "* piouuu piouuu piouuu *" << std::endl;
	return;
}
