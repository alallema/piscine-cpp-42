/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 19:18:14 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 22:31:53 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy(int hp, std::string const & type) : _type(type), _hp(hp){
//	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

Enemy::Enemy(Enemy const & src){
//	std::cout << "Copy Constructor called" << std::endl;
	*this = src;
	return;
}

Enemy::~Enemy(void) {
//	std::cout << "Destructor called" << std::endl;
	return;
}

std::string		Enemy::getType(void) const{
	return(this->_type);
}

int				Enemy::getHP(void) const{
	return(this->_hp);
}

void			Enemy::setType(std::string type){
	this->_type = type;
	return;
}

void			Enemy::setHP(int hp){
	this->_hp = hp;
	return;
}

void			Enemy::takeDamage(int damage) {
	if (damage > 0)
	{
		if (this->getHP() - damage >= 0)
			this->setHP(this->getHP() - damage);
		else
			this->setHP(0);
	}
	return;
}
