/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 21:13:48 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 21:18:02 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RADSCORPION_HPP
# define RADSCORPION_HPP
#include <iostream>
#include "Enemy.hpp"

class	RadScorpion : public Enemy{

public:
	RadScorpion(void);
	RadScorpion(RadScorpion const & src);
	virtual ~RadScorpion(void);

	RadScorpion &			operator=(RadScorpion const & rhs);

private:
//	std::string		_str;
};

#endif
