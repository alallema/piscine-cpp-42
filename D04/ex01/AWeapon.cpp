/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 18:49:24 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 19:25:56 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon(std::string const & name, int apcost, int damage) : _name(name), _apcost(apcost), _damage(damage) {
	std::cout << "Parametric Constructor called" << std::endl;
	return;
}

AWeapon::AWeapon(AWeapon const & src){
	std::cout << "Copy Constructor called" << std::endl;
	*this = src;
	return;
}

AWeapon::~AWeapon(void) {
	std::cout << "Destructor called" << std::endl;
	return;
}

std::string		AWeapon::getName(void) const{
	return(this->_name);
}

int		AWeapon::getAPCost(void) const{
	return(this->_apcost);
}

int		AWeapon::getDamage(void) const{
	return(this->_damage);
}

void	AWeapon::setName(std::string name){
	this->_name = name;
	return;
}

void	AWeapon::setAPCost(int apcost){
	this->_apcost = apcost;
	return;
}

void	AWeapon::setDamage(int damage){
	this->_damage = damage;
	return;
}
