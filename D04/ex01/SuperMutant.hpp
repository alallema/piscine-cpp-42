/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 21:03:45 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 21:12:36 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERMUTANT_HPP
# define SUPERMUTANT_HPP
#include <iostream>
#include "Enemy.hpp"

class	SuperMutant : public Enemy {

public:
	SuperMutant(void);
	SuperMutant(SuperMutant const & src);
	virtual ~SuperMutant(void);

	SuperMutant &	operator=(SuperMutant const & rhs);
	virtual void	takeDamage(int);

private:
//	std::string		_str;
};

#endif
