/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 18:54:23 by alallema          #+#    #+#             */
/*   Updated: 2018/03/30 19:09:31 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POWERFIST_HPP
# define POWERFIST_HPP
#include <iostream>
#include "AWeapon.hpp"

class	PowerFist : public AWeapon{

public:
	PowerFist(void);
	PowerFist(PowerFist const & src);
	virtual ~PowerFist(void);

	PowerFist &		operator=(PowerFist const & rhs);
	void			attack() const;
//	std::string		getstr(void) const;

private:
//	std::string		_str;
};

#endif
