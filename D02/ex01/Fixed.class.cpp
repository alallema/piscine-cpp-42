/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/28 15:33:00 by alallema          #+#    #+#             */
/*   Updated: 2018/03/28 16:55:14 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.class.hpp"

Fixed::Fixed(void) : _n(0){
	std::cout << "Default constructor called" << std::endl;
	return;
}

Fixed::Fixed(int const n) : _n(n){
	std::cout << "Int constructor called" << std::endl;
	this->_n = n << this->_bit;
	return;
}

Fixed::Fixed(float const n) : _n(n){
	std::cout << "Float constructor called" << std::endl;
	this->_n = roundf(n * (1 << this->_bit));
	return;
}

Fixed::Fixed(Fixed const & src){
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
	return;
}

Fixed::~Fixed(void) {
	std::cout << "Destructor called" << std::endl;
	return;
}

Fixed &		Fixed::operator=(Fixed const & rhs) {
	std::cout << "Assignation operator called" << std::endl;
	this->setRawBits(rhs.getRawBits());
	return(*this);
}

int		Fixed::getRawBits(void) const{
	return(this->_n);
}

void	Fixed::setRawBits(int const raw){
	this->_n = raw;
	return;
}

int		Fixed::toInt(void) const{
	return (this->_n >> this->_bit);
}

float	Fixed::toFloat(void) const{
	return ((float)this->_n / (1 << this->_bit));
}

std::ostream &		operator<<(std::ostream & o, Fixed const & rhs) {
	o << rhs.toFloat();
	return(o);
}

const int Fixed::_bit = 8;
