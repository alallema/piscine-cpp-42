/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/28 15:33:00 by alallema          #+#    #+#             */
/*   Updated: 2018/03/28 19:10:25 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.class.hpp"

Fixed::Fixed(void) : _n(0){
	return;
}

Fixed::Fixed(int const n) : _n(n){
	this->_n = n << this->_bit;
	return;
}

Fixed::Fixed(float const n) : _n(n){
	this->_n = roundf(n * (1 << this->_bit));
	return;
}

Fixed::Fixed(Fixed const & src){
	*this = src;
	return;
}

Fixed::~Fixed(void) {
	return;
}

Fixed &		Fixed::operator=(Fixed const & rhs) {
	this->setRawBits(rhs.getRawBits());
	return(*this);
}

bool		Fixed::operator>(Fixed const & rhs) const {
	return(this->_n > rhs.getRawBits());
}

bool		Fixed::operator<(Fixed const & rhs) const{
	return(this->_n < rhs.getRawBits());
}

bool		Fixed::operator>=(Fixed const & rhs) const{
	return(this->_n >= rhs.getRawBits());
}
bool		Fixed::operator<=(Fixed const & rhs) const {
	return(this->_n <= rhs.getRawBits());
}

bool		Fixed::operator==(Fixed const & rhs) const {
	return(this->_n == rhs.getRawBits());
}

bool		Fixed::operator!=(Fixed const & rhs) const {
	return(this->_n != rhs.getRawBits());
}

Fixed		Fixed::operator+(Fixed const & rhs) {
	return(this->toFloat() + rhs.toFloat());
}

Fixed		Fixed::operator-(Fixed const & rhs) {
	return(this->toFloat() - rhs.toFloat());
}

Fixed		Fixed::operator*(Fixed const & rhs) {
	return(this->toFloat() * rhs.toFloat());
}

Fixed		Fixed::operator/(Fixed const & rhs) {
	if (rhs.toFloat() == 0) {
		std::cout << "No division by zero" << std::endl;
		return Fixed();
	}
	else {
		return(this->toFloat() / rhs.toFloat());
	}
}

Fixed &			Fixed::operator++(void){
	this->_n += 1;
	return (*this);
}

Fixed			Fixed::operator++(int){
	Fixed tmp(*this);
	operator++();
	return (tmp);
}

Fixed &			min(Fixed & val1, Fixed & val2){
	if (val1.getRawBits() <= val2.getRawBits())
		return (val1);
	else
		return (val2);
}

Fixed &			max(Fixed & val1, Fixed & val2){
	if (val1.getRawBits() >= val2.getRawBits())
		return (val1);
	else
		return (val2);
}

Fixed const & 	Fixed::min(Fixed const & val1, Fixed const & val2){
	if (val1 <= val2)
		return (val1);
	else
		return (val2);
}

Fixed const & 	Fixed::max(Fixed const & val1, Fixed const & val2){
	if (val1 >= val2)
		return (val1);
	else
		return (val2);
}

int		Fixed::getRawBits(void) const{
	return(this->_n);
}

void	Fixed::setRawBits(int const raw){
	this->_n = raw;
	return;
}

int		Fixed::toInt(void) const{
	return (this->_n >> this->_bit);
}

float	Fixed::toFloat(void) const{
	return ((float)this->_n / (1 << this->_bit));
}

std::ostream &		operator<<(std::ostream & o, Fixed const & rhs) {
	o << rhs.toFloat();
	return(o);
}

const int Fixed::_bit = 8;
