/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/28 15:36:01 by alallema          #+#    #+#             */
/*   Updated: 2018/03/28 19:05:42 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP
#include <iostream>
#include <cmath>

class	Fixed {

public:
	Fixed(void);
	Fixed(int const n);
	Fixed(float const n);
	Fixed(Fixed const & src);
	~Fixed(void);

	Fixed &			operator=(Fixed const & rhs);

	bool			operator>(Fixed const & rhs) const;
	bool			operator<(Fixed const & rhs) const;
	bool			operator>=(Fixed const & rhs) const;
	bool			operator<=(Fixed const & rhs) const;
	bool			operator==(Fixed const & rhs) const;
	bool			operator!=(Fixed const & rhs) const;

	Fixed			operator+(Fixed const & rhs);
	Fixed			operator-(Fixed const & rhs);
	Fixed			operator*(Fixed const & rhs);
	Fixed			operator/(Fixed const & rhs);

	int				toInt(void) const;
	float			toFloat(void) const;
	int				getRawBits() const;
	void			setRawBits( int const raw );

	static Fixed &			min(Fixed & val1, Fixed & val2);
	static Fixed &			max(Fixed & val1, Fixed & val2);

	static Fixed const & 	min(Fixed const & val1, Fixed const & val2);
	static Fixed const & 	max(Fixed const & val1, Fixed const & val2);

	Fixed &			operator++(void);
	Fixed			operator++(int);

private:
	int					_n;
	static const int	_bit;
};

std::ostream & operator<<(std::ostream & o, Fixed const & rhs);

#endif
